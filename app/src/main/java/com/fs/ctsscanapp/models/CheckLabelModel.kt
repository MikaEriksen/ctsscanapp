// To parse the JSON, install jackson-module-kotlin and do:
//
//   val checkLabelModel = CheckLabelModel.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.*
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.module.kotlin.*

val mapperCheckLabel: ObjectMapper = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class CheckLabelModel (
    val message: String? = null,
    val data: Data3? = null
) {
    fun toJson() = mapperCheckLabel.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapperCheckLabel.readValue<CheckLabelModel>(json)
    }
}

data class Data3 (
    val status: Boolean? = null,
    val opmerking: String? = null
)