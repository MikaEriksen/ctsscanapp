package com.fs.ctsscanapp.activities.tms


import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.Unload
import com.fs.ctsscanapp.services.SoapUtils
import kotlinx.android.synthetic.main.activity_rit.*
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.xml.sax.InputSource
import java.io.StringReader
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory


class RitActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rit)

        Unload.Rit?.let { getZendingen() }
        ritnummerTextView.text = Unload.Rit.toString()
        unscannedradioButton.performClick()
    }


    fun setContent(ritnr: Long) {
        Unload.Rit = ritnr
    }


    private fun getZendingen() {
        AsyncTask.execute {
            val result = Unload.Rit?.let { SoapUtils.getZendingen(it, "LossenInLoods") }
            val dbFactory: DocumentBuilderFactory = DocumentBuilderFactory.newInstance()
            val dBuilder: DocumentBuilder = dbFactory.newDocumentBuilder()
            val xmlInput = InputSource(StringReader(result!!))
            val doc: Document = dBuilder.parse(xmlInput)

            Unload.GetZendingenResult =
                doc.getElementsByTagName("GetZendingenResult").item(0) as Element

            runOnUiThread {
                unscannedradioButton.performClick()
            }
        }
    }


    fun onClickBack(view: View) {
        val myIntent = Intent(
            this@RitActivity,
            LosActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@RitActivity.startActivity(myIntent)
    }


    fun onClickUnscanned(view: View) {
        //TODO Aantal Colli vs AantalGelost

        //show finish
        button44.visibility = Button.VISIBLE

        //show unscanned zendingen in table
        val zendingInfos = Unload.GetZendingenResult?.childNodes
        if (zendingInfos != null) {
            clearAndCreateHeader()

            for (j in 0 until zendingInfos.length) {

                val zendingInfo: Element = zendingInfos.item(j) as Element

                if (zendingInfo.getElementsByTagName("Status")
                        .item(0).firstChild.nodeValue.toInt() < 405
                ) {
                    createZendingRow(zendingInfo)
                }
            }
        }
    }


    private fun clearAndCreateHeader() {
        val ll = findViewById<LinearLayout>(R.id.zendingenLL)
        ll.removeAllViews()

        val tableRow_821 = LinearLayout(this)
        tableRow_821.orientation = LinearLayout.HORIZONTAL
        tableRow_821.setBackgroundColor(Color.GRAY)
        val layout_665 = TableRow.LayoutParams(
            TableRow.LayoutParams.MATCH_PARENT,
            TableRow.LayoutParams.WRAP_CONTENT
        )
        tableRow_821.layoutParams = layout_665

        val textView82 = TextView(this)
        textView82.text = "Consignment"
        textView82.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView82.setTextColor(Color.WHITE)
        val layout_951 = TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT)
        layout_951.marginStart = 8
        layout_951.topMargin = 4
        layout_951.marginEnd = 8
        layout_951.bottomMargin = 4
        layout_951.weight = 5F
        textView82.layoutParams = layout_951
        tableRow_821.addView(textView82, 0)

        val textView83 = TextView(this)
        textView83.text = "Zone"
        textView83.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView83.setTextColor(Color.WHITE)
        val layout_459 = TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT)
        layout_459.marginStart = 8
        layout_459.topMargin = 4
        layout_459.marginEnd = 8
        layout_459.bottomMargin = 4
        layout_459.weight = 2F
        textView83.layoutParams = layout_459
        tableRow_821.addView(textView83, 1)

        val textView84 = TextView(this)
        textView84.text = "City"
        textView84.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView84.setTextColor(Color.WHITE)
        textView84.layoutParams = layout_951
        tableRow_821.addView(textView84, 2)

        val textView85 = TextView(this)
        textView85.text = "Colli"
        textView85.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView85.setTextColor(Color.WHITE)
        textView85.layoutParams = layout_459
        tableRow_821.addView(textView85, 3)

        ll.addView(tableRow_821, 0)
    }


    private fun setLParams(weight: Float): LinearLayout.LayoutParams {
        val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        lp.marginStart = 12
        lp.topMargin = 4
        lp.marginEnd = 12
        lp.bottomMargin = 4
        lp.weight = weight
        return lp
    }


    private fun createZendingRow(zendingInfo: Element) {
        val ll = findViewById<LinearLayout>(R.id.zendingenLL)

        //declare vars
        val zendingnummer = getElementSafe(zendingInfo, "ZendingNummer")
        val zoneLossen = getElementSafe(zendingInfo, "ZoneLossen")
        val ontvangerPlaats = getElementSafe(zendingInfo, "OntvangerPlaats")
        val laadInstructie = getElementSafe(zendingInfo, "LaadInstructie")
        val status = getElementSafe(zendingInfo, "Status")
        var colliAantal = getElementSafe(zendingInfo, "ColliAantal")
        val isHeeftPrioriteit = getElementSafe(zendingInfo, "IsHeeftPrioriteit")

        colliAantal = if (status.toInt() < 405 || status.toInt() == 900) {
            "0/$colliAantal"
        } else {
            "$colliAantal/$colliAantal"
        }

        val tableRow463 = LinearLayout(this)
        tableRow463.orientation = LinearLayout.HORIZONTAL
        val layout665 = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        tableRow463.layoutParams = layout665
        if (isHeeftPrioriteit == "true")
            tableRow463.background = getDrawable(R.color.colorPrio)

        val textView86 = TextView(this)
        textView86.text = zendingnummer
        textView86.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView86.setTextColor(Color.WHITE)
        isGroen(textView86, status)
        textView86.layoutParams = setLParams(5F)
        tableRow463.addView(textView86)

        val textView87 = TextView(this)
        textView87.text = zoneLossen
        textView87.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView87.setTextColor(Color.WHITE)
        textView87.layoutParams = setLParams(2F)
        tableRow463.addView(textView87)

        val textView88 = TextView(this)
        if (laadInstructie.isNotBlank())
            textView88.background = getDrawable(R.color.colorOrange)
        textView88.ellipsize = TextUtils.TruncateAt.END
        textView88.maxLines = 1
        textView88.text = ontvangerPlaats
        textView88.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView88.setTextColor(Color.WHITE)
        textView88.layoutParams = setLParams(5F)
        tableRow463.addView(textView88)

        val textView92 = TextView(this)
        textView92.text = colliAantal
        textView92.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textView92.setTextColor(Color.WHITE)
        if (status.toInt() == 900 || status.toInt() < 405) {
            textView92.setTextColor(Color.RED)
        }
        textView92.layoutParams = setLParams(2F)
        tableRow463.addView(textView92)

        when (isHeeftPrioriteit) {
            "true" -> {
                ll.addView(tableRow463, 1)
            }
            else -> {
                ll.addView(tableRow463)
            }
        }
    }


    private fun isGroen(tv: TextView, status: String) {
        if (status.toInt() > 405
        ) {
            tv.setTextColor(Color.GREEN)
        }
    }


    fun onClickRefresh(view: View) {
        AsyncTask.execute {
            getZendingen()
        }
    }


    private fun getElementSafe(el: Element, element: String): String {
        return if (el.getElementsByTagName(element) == null) {
            ""
        } else {
            if (el.getElementsByTagName(element).item(0) == null) {
                ""
            } else {
                if (el.getElementsByTagName(element).item(0).firstChild == null) {
                    ""
                } else {
                    el.getElementsByTagName(element).item(0).firstChild.nodeValue
                }
            }
        }
    }


    fun onClickScanned(view: View) {
        //hide finish
        button44.visibility = Button.GONE

        //show gescande zendingen in table
        val zendingInfos = Unload.GetZendingenResult?.childNodes
        if (zendingInfos != null) {
            clearAndCreateHeader()

            for (j in 0 until zendingInfos.length) {
                val zendingInfo: Element = zendingInfos.item(j) as Element
                val status =
                    if (getElementSafe(zendingInfo, "Status").isEmpty()) "0" else getElementSafe(
                        zendingInfo,
                        "Status"
                    )
                if (status.toInt() >= 405
                ) {
                    createZendingRow(zendingInfo)
                }
            }
        }
    }
}