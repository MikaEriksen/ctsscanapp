package com.fs.ctsscanapp.models;

import com.fasterxml.jackson.annotation.*;

public class AuthRes {
    private boolean status;
    private String opmerking;

    @JsonProperty("status")
    public boolean getStatus() { return status; }
    @JsonProperty("status")
    public void setStatus(boolean value) { this.status = value; }

    @JsonProperty("opmerking")
    public String getOpmerking() { return opmerking; }
    @JsonProperty("opmerking")
    public void setOpmerking(String value) { this.opmerking = value; }
}