package com.fs.ctsscanapp.activities.container

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.activities.MenuActivity
import kotlinx.android.synthetic.main.activity_container_menu.*

class ContainerMenu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container_menu)
        runOnUiThread {
            textView47.text = UserName.user
        }
    }

    fun onClickStartScanning(view: View) {
        val myIntent = Intent(
            this,
            ContainerClient::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }

    fun onClickBackToHome(view: View) {
        val myIntent = Intent(
            this,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }
}
