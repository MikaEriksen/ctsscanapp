package main.libraries;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import oracle.jdbc.driver.OracleDriver;

public class CTSOracle {
	private String db_ip = null;
	private Connection conn = null;
	
	public CTSOracle(){
		this.db_ip = "192.168.15.24";
	}

	public boolean connect(String db){
		try {
			DriverManager.registerDriver(new OracleDriver());
			conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.12.165:1521:kwldb","life60","inter00");
            //Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();

			//conn = DriverManager.getConnection("jdbc:oracle:thin:life60/inter00@"+db_ip+":1521:"+db);
			
			if(conn == null && conn.isClosed()) return false;
	    	
			return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
	}
	
	public ResultSet query(String query){
		try{
			Statement s = conn.createStatement();
			return s.executeQuery(query);
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public int update(String query){
		try{
			Statement s = conn.createStatement();
			return s.executeUpdate(query);
		} catch (Exception e){
			e.printStackTrace();
			return 0;
		}
	}
	
	public List<Map<String,Object>> getListMap(ResultSet rs, List<String> force_string){
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		
		//Return if null
		if(rs == null){ return null; }
		
		try {
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			
			while (rs.next()) {
		        Map<String,Object> row = Collections.synchronizedMap(new LinkedHashMap<String, Object>(columns));
		        for(int i=1; i<=columns; ++i) {
		        	if(force_string != null && force_string.contains(md.getColumnLabel(i))){
		        		row.put(md.getColumnLabel(i),rs.getString(i));
		        	}else{
		        		row.put(md.getColumnLabel(i),rs.getObject(i));
		        	}
		        }
		        list.add(row);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public List<Map<String,Object>> getListMap(ResultSet rs, List<String> force_string, List<String> force_xml){
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		
		//Return if null
		if(rs == null){ return null; }
		
		try {
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			
			while (rs.next()) {
		        Map<String,Object> row = Collections.synchronizedMap(new LinkedHashMap<String, Object>(columns));
		        for(int i=1; i<=columns; ++i) {
		        	if(force_string != null && force_string.contains(md.getColumnLabel(i))){
		        		row.put(md.getColumnLabel(i),rs.getString(i));
		        	}else if(force_xml != null && force_xml.contains(md.getColumnLabel(i))){
		        		Array rsa = rs.getArray(i);
		        		String[] rsao = (String[]) rsa.getArray();
		        		row.put(md.getColumnLabel(i),rsao);
		        	}else{
		        		row.put(md.getColumnLabel(i),rs.getObject(i));
		        	}
		        }
		        list.add(row);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Map<String,Object> getMap(ResultSet rs, boolean as_string){
		Map<String,Object> map = new HashMap<String,Object>();
		
		//Return if null
		if(rs == null){ return null; }
		
		try {
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			
			if(!rs.next()){
				return map;
			}
			
	        map = Collections.synchronizedMap(new LinkedHashMap<String, Object>(columns));
	        for(int i=1; i<=columns; ++i) {
	            map.put(md.getColumnLabel(i),( as_string ? rs.getString(i) : rs.getObject(i)));
	        }
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	public Map<String,Object> getSingleMap(ResultSet rs){		
		//Return if null
		if(rs == null){ return null; }
		
		try {
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			
			while (rs.next()) {
		        Map<String,Object> row = Collections.synchronizedMap(new LinkedHashMap<String, Object>(columns));
		        for(int i=1; i<=columns; ++i) {
		            row.put(md.getColumnLabel(i),rs.getObject(i));
		        }
		        return row;
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new HashMap<String,Object>();
	}
	
	public Connection getConnection(){
		return this.conn;
	}
	
	public boolean close(){
		try {
			if(conn != null && !conn.isClosed()){
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
