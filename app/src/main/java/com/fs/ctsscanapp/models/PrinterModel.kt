// To parse the JSON, install jackson-module-kotlin and do:
//
//   val printerModel = PrinterModel.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.*
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.module.kotlin.*

val mapperPm = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class PrinterModel (
    val message: String? = null,
    val printerNames: List<String>? = null
) {
    fun toJson() = mapperPm.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapperPm.readValue<PrinterModel>(json)
    }
}
