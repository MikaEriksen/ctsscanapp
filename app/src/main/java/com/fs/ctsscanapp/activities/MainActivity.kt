package com.fs.ctsscanapp.activities

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.AuthRes
import com.fs.ctsscanapp.services.ConverterAuth.fromJsonString
import com.fs.ctsscanapp.services.TMS_WMS
import com.fs.ctsscanapp.services.auth
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val passwordfield: EditText = findViewById<View>(R.id.editText3) as EditText
        passwordfield.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                button2.performClick()
                return@OnEditorActionListener true
            }
            false
        })
    }


    fun onClickLoginButton(view: View) {

        if (editText2.text.toString().isBlank() || editText3.text.toString().isBlank()) {
            Toast.makeText(this, "Fields can't be empty", Toast.LENGTH_LONG).show()
            return
        }

        if (!checkConnectivity())
            return

        try {
            Toast.makeText(
                this,
                "Logging in...",
                Toast.LENGTH_SHORT
            ).show()
            AsyncTask.execute {
                val response = auth().rheaAuth(
                    editText2.text.toString(),
                    editText3.text.toString()
                )

                UserName.user = editText2.text.toString()

                val authResponse: AuthRes = fromJsonString(response)
                if (authResponse.status) {
                    runOnUiThread {
                        editText2.setText("")
                        editText3.setText("")
                    }
                    //Laat volgend scherm zien
                    val myIntent = Intent(
                        this@MainActivity,
                        //MenuActivity::class.java
                        TMS_WMS::class.java
                        //FrameActivity::class.java
                    )
                    this@MainActivity.startActivity(myIntent)
                } else {
                    runOnUiThread {
                        Toast.makeText(
                            this@MainActivity,
                            authResponse.opmerking.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return
    }


    private fun checkConnectivity(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetwork
        val isConnected = activeNetwork != null
        if (!isConnected) {
            Toast.makeText(this, "Check network", Toast.LENGTH_LONG).show()
        }
        return isConnected
    }
}
