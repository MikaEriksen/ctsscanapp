package com.fs.ctsscanapp

import org.w3c.dom.Element

object UserName {
    var user: String = "unknown"
}

object Barcode {
    var dosvlg: String? = null
}

object Unload{
    var Date: String? = null
    var Rit: Long? = null
    var Ritten: Element? = null
    var GetZendingenResult: Element? = null
}

object LoodsCheck{
    var ZendingNummer: Long? = null
    var Zending: Element? = null
}

object MenuItem {
    var selected: String? = null
    var TmsOrWms: String? = null
    var warehouse: String? = null
    var inclocation: Boolean = true
    var scannedLocation: String? = null
}

object Photo {
    var currentPhotoPath: String = ""
    const val REQUEST_IMAGE_CAPTURE = 1
    const val REQUEST_TAKE_PHOTO = 1
}

object Pallet {
    var boxcount: Long? = null
}

object Container{
    var clientid: Long? = null
    var client: String? = null
    var containernumber: String? = null
    var ponumber: String? = null
    var containerid: Long? = null
    var pallet: String? = null
    var palletid: Long? = null
    var childid: String? = null
    var ean: String? = null
    var fnumber: String? = null
    var boxid: Long? = null
    var mixed: Boolean? = null
    var labelsreprinted: Long? = null
    var damaged: Boolean? = null
    var message: String? = null
    var remark: String? = "-"
}