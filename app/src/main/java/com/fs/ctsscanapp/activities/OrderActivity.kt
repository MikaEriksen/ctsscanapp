package com.fs.ctsscanapp.activities

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.isDigitsOnly
import com.fs.ctsscanapp.Barcode
import com.fs.ctsscanapp.MenuItem
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.Ean
import com.fs.ctsscanapp.models.EanList
import com.fs.ctsscanapp.models.Response
import com.fs.ctsscanapp.models.Zending
import com.fs.ctsscanapp.services.ConverterData
import com.fs.ctsscanapp.services.Japi
import com.fs.ctsscanapp.services.Rhea
import kotlinx.android.synthetic.main.activity_order.*
import org.json.JSONObject


class OrderActivity : AppCompatActivity() {

    private lateinit var zending: Zending
    private var eanHashMap: HashMap<String, String> = HashMap()

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        //show activity
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        runOnUiThread {
            textView18.text = UserName.user
        }

        AsyncTask.execute {
            try {
                //get data
                val jsonString: String? = Barcode.dosvlg?.let {
                    Japi().LoadTruckGetShipment(
                        it
                    )
                }
                zending =
                    jsonString?.let { Zending.fromJson(it) }!!

                if (!zending.status) {
                    runOnUiThread {
                        Toast.makeText(this, zending.opmerking, Toast.LENGTH_SHORT).show()
                    }
                    return@execute
                }

                //fill eanHash
                zending.data?.eanList?.forEach { it ->
                    it.ean?.forEach { itt ->
                        it.artkd?.let { it1 -> itt.value?.let { it2 -> eanHashMap[it2] = it1 } }
                    }
                }

                //vul data
                runOnUiThread {

                    var totalCount = 0

                    when (MenuItem.selected) {
                        "tl" -> {
                            totalCount = zending.data?.doslist?.lastIndex?.plus(1)!!
                        }
                        "cc" -> {
                            totalCount = zending.data?.sscclist?.lastIndex?.plus(1)!!
                        }
                        "co" -> {
                            zending.data?.eanList?.forEach { it: EanList ->
                                totalCount += it.count!!.toInt()
                            }
                        }
                    }

                    textView4.text = String.format(
                        "0/%d Colli: %s %dKG",
                        totalCount,
                        zending.data?.colkd,
                        zending.data?.brgew
                    )

                    textView8.text = zending.data?.dosvlg.toString()
                    Barcode.dosvlg = zending.data?.dosvlg.toString()

                    textView10.text = String.format(
                        //Voornaam Achternaam
                        //Straat Nummer
                        //Postcode Stad
                        //Land
                        "%s\n%s\n%s %s\n%s",
                        zending.data?.shiptonaam,
                        zending.data?.shiptoadres,
                        zending.data?.shiptopostcode,
                        zending.data?.shiptostad,
                        zending.data?.shiptoland
                    )

                    textView31.text = zending.data?.lds

                    if (zending.data?.lds == "") {
                        textView31.visibility = TextView.GONE
                        divider4.visibility = View.GONE
                    }

                    if (MenuItem.selected == "tl" || MenuItem.selected == "co") {
                        button.visibility = Button.VISIBLE
                    } else {
                        button.visibility = Button.GONE
                    }
                }

                when (MenuItem.selected) {
                    "cc" -> {
                        zending.data?.sscclist?.forEach { it: String ->
                            val ll: LinearLayout = findViewById(R.id.zendingenList)
                            val textView = TextView(this)
                            textView.layoutParams =
                                LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                                )
                            textView.setTextColor(ColorStateList.valueOf(Color.WHITE))
                            textView.textSize = 24f
                            textView.text = it
                            textView.tag = it

                            runOnUiThread {
                                ll.addView(textView)
                            }
                        }
                    }
                    "tl" -> {
                        zending.data?.doslist?.forEach { it: Long ->
                            val ll: LinearLayout = findViewById(R.id.zendingenList)
                            val textView = TextView(this)
                            textView.layoutParams =
                                LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                                )
                            textView.setTextColor(ColorStateList.valueOf(Color.WHITE))
                            textView.textSize = 24f
                            textView.text = it.toString()
                            textView.tag = it

                            runOnUiThread {
                                ll.addView(textView)
                            }
                        }
                    }
                    "co" -> {
                        zending.data?.eanList?.forEach { it: EanList ->
                            val ll: LinearLayout = findViewById(R.id.zendingenList)
                            val textView = TextView(this)
                            textView.layoutParams =
                                LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                                )
                            textView.setTextColor(ColorStateList.valueOf(Color.WHITE))
                            textView.textSize = 24f
                            textView.text = String.format("%s - 0/%d", it.artkd, it.count)
                            textView.tag = it.artkd

                            runOnUiThread {
                                ll.addView(textView)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                runOnUiThread {
                    Toast.makeText(this, "Go back and try again please.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @Suppress("DEPRECATION")
    override fun dispatchKeyEvent(eventsscc: KeyEvent): Boolean {

        if (eventsscc.characters.isNullOrEmpty())
            return super.dispatchKeyEvent(eventsscc)

        var scannedBarcode = eventsscc.characters.trim()
        var count: Long? = 0

        try {
            when (MenuItem.selected) {
                "co" -> {
                    try {
                        if (scannedBarcode.isDigitsOnly()){
                            zending.data?.eanList?.forEach { eanList: EanList ->
                                eanList.ean?.forEach { ean: Ean ->
                                    if (ean.value?.equals(scannedBarcode) == true){
                                        count = ean.count
                                    }
                                }
                            }
                            scannedBarcode = eanHashMap[scannedBarcode].toString()
                        }

                        val view = this.zendingenList.findViewWithTag<TextView>(scannedBarcode)
                        runOnUiThread {

                            //set new aantal
                            val currentText = view.text.toString()
                            val currentCount =
                                view.text.toString().substringAfter(" - ").substringBefore("/")
                                    .toInt()

                            view.text = String.format(
                                "%s - %d/%d",
                                currentText.substringBefore(" - "),
                                currentCount + count!!,
                                currentText.substringAfter("/").toInt()
                            )

                            if (view.text.toString().substringAfter(" - ").substringBefore("/").toInt() ==
                                view.text.toString().substringAfter("/").toInt()
                            ) {
                                view.setTextColor(ColorStateList.valueOf(Color.GREEN))
                            } else if (view.text.toString().substringAfter(" - ").substringBefore("/").toInt() >
                                view.text.toString().substringAfter("/").toInt()
                            ) {
                                view.setTextColor(ColorStateList.valueOf(Color.RED))
                            }

                            setCountCompleted(countCompleted()) //text bovenin

                            if (countCompleted() == zendingenList.childCount) {
                                button.text = "Finish Complete"
                                button.backgroundTintList =
                                    ColorStateList.valueOf(Color.rgb(16, 173, 24))
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        runOnUiThread {
                            Toast.makeText(this, "Not on the list", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                "cc" -> {
                    try {
                        val sscc = this.zendingenList.findViewWithTag<TextView>(scannedBarcode.toLong())
                        runOnUiThread {
                            if (sscc.textColors != ColorStateList.valueOf(Color.GREEN)) {
                                sscc.setTextColor(ColorStateList.valueOf(Color.GREEN))
                                sscc.append(" OK")
                                setCountCompleted(countCompleted())
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        runOnUiThread {
                            Toast.makeText(this, "Not on the list", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                "tl" -> {
                    try {
                        val sscc = this.zendingenList.findViewWithTag<TextView>(scannedBarcode.toLong())
                        runOnUiThread {
                            if (sscc.textColors != ColorStateList.valueOf(Color.GREEN)) {
                                sscc.setTextColor(ColorStateList.valueOf(Color.GREEN))
                                sscc.append(" OK")
                                setCountCompleted(countCompleted())
                            }

                            if (countCompleted() == zendingenList.childCount) {
                                button.text = "Finish Complete"
                                button.backgroundTintList =
                                    ColorStateList.valueOf(Color.rgb(16, 173, 24))
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        runOnUiThread {
                            Toast.makeText(this, "Not on the list", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return super.dispatchKeyEvent(eventsscc)
    }

    private fun setCountCompleted(i: Int) {
        val old = textView4.text.toString()

        val sb = StringBuilder(old)
        sb.deleteCharAt(0)

        val newtext = String.format("%d/%s", i, old.substringAfter("/"))

        textView4.text = newtext

        var color: ColorStateList = ColorStateList.valueOf(Color.GREEN)

        try {
            for (index in 0 until zendingenList.childCount) {
                val code: TextView = zendingenList.getChildAt(index) as TextView

                if (code.textColors == ColorStateList.valueOf(Color.RED)) {
                    color = ColorStateList.valueOf(Color.RED)
                    //throw IllegalAccessError("Color")
                }

                if (code.textColors == ColorStateList.valueOf(getColor(R.color.colorPrimaryLight))) {
                    color = ColorStateList.valueOf(getColor(R.color.colorPrimaryLight))
                    //throw IllegalAccessError("Color")
                }
            }
        } catch (e: IllegalAccessError) {
            e.printStackTrace()
        }

        textView4.setTextColor(color)
    }

    private fun countCompleted(): Int {
        var count = 0

        when (MenuItem.selected) {
            "tl", "cc" -> {
                try {
                    for (i in 0 until zendingenList.childCount) {
                        val code: TextView = zendingenList.getChildAt(i) as TextView

                        if (code.textColors == ColorStateList.valueOf(Color.GREEN)) {
                            count++
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    return count
                }
            }

            "co" -> {
                try {
                    for (i in 0 until zendingenList.childCount) {
                        val view: TextView = zendingenList.getChildAt(i) as TextView

                        val currentCount =
                            view.text.toString().substringAfter(" - ").substringBefore("/").toInt()
                        count += currentCount
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    return count
                }
            }
        }
        return count
    }

    fun onClickFinish(view: View) {
        AsyncTask.execute {

            when (MenuItem.selected) {
                "tl", "cc" -> {
                    val response: String = Rhea().sendFinish(
                        Barcode.dosvlg,
                        completed().toString(),
                        countCompleted().toString(),
                        UserName.user,
                        zending.data?.department
                    )

                    val rf: Response = ConverterData.fromJsonString(response)

                    if (rf.status) {
                        val myIntent = Intent(
                            this@OrderActivity,
                            ScanActivity::class.java
                        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        this@OrderActivity.startActivity(myIntent)
                    } else {
                        runOnUiThread {
                            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                "co" -> {
                    //Add TnT
                    val response: String? = Barcode.dosvlg?.let { Japi().addTnT(it, if (completed()) "CHK SCAND" else "CHK FAULT", UserName.user) }

                    val responseJson = JSONObject(response!!)
                    val data = responseJson.getJSONObject("data")

                    if (data.getBoolean("status")) {
                        runOnUiThread {
                            val myIntent = Intent(
                                this@OrderActivity,
                                ScanActivity::class.java
                            ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                            this@OrderActivity.startActivity(myIntent)
                        }
                    } else {
                        runOnUiThread {
                            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

    private fun completed(): Boolean {
        return countCompleted() == zendingenList.childCount
    }

    fun onClickBackHome(view: View) {
        val myIntent = Intent(
            this@OrderActivity,
            ScanActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@OrderActivity.startActivity(myIntent)
    }
}
