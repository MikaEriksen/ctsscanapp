// To parse the JSON, install Klaxon and do:
//
//   val responseModel = ResponseModel.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.beust.klaxon.*

private val klaxon = Klaxon()

data class ResponseModel (
    val message: String? = null,
    val data: List<DatumPallets>? = null
) {
    fun toJson() = klaxon.toJsonString(this)

    companion object {
        fun fromJson(json: String) = klaxon.parse<ResponseModel>(json)
    }
}

data class DatumPallets (
    @Json(name = "pallets_aantal_afgerond")
    val palletsAantalAfgerond: Long? = null,

    @Json(name = "pallets_aantal_totaal")
    val palletsAantalTotaal: Long? = null,

    @Json(name = "pallet_gescandeId")
    val palletGescandeID: Long? = null
)
