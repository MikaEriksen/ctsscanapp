package com.fs.ctsscanapp.activities.container

import android.content.Intent
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.AsyncTask
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.Container
import com.fs.ctsscanapp.Pallet
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.DefaultID
import com.fs.ctsscanapp.services.Japi
import kotlinx.android.synthetic.main.activity_container_pallet.*

class ContainerPallet : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container_pallet)

        runOnUiThread {
            textView49.text = UserName.user
            textView72.text = Container.client
            textView67.text = "Container: " + Container.containernumber
            textView102.text = "PO: " + Container.ponumber
            //textView83.text = "Total on pallet: ${Pallet.boxcount?: "-"}"
        }
    }


    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        try {
            if (event.keyCode == KeyEvent.KEYCODE_ENTER) {
                button29.performClick()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            return super.dispatchKeyEvent(event)
        }
    }


    private fun checkScanAllowed(text: String): Boolean {
        return Regex("(CTS-)(\\d{3}|\\d{5})\$").matches(text) || Regex("^21\\d{12}$").matches(text)
    }


    fun onClickToScanning(view: View) {
        try {
            if (checkScanAllowed(textViewCheckLocation.text.toString())) {
                Container.pallet = textViewCheckLocation.text.toString()

                AsyncTask.execute {
                    val response: String = Japi().sendPallet()
                    val defaultid: DefaultID = response.let { DefaultID.fromJson(it) }
                    Container.palletid = defaultid.data?.id
                    Container.mixed = defaultid.data?.mixed
                    Pallet.boxcount = defaultid.data?.boxCount

                    runOnUiThread {
                        if (Container.palletid == null){
                            Toast.makeText(this, "Possible duplicate, can't continue", Toast.LENGTH_LONG).show()
                            return@runOnUiThread
                        }

                        val myIntent = Intent(
                            this,
                            ContainerEanBox::class.java
                        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        this.startActivity(myIntent)
                    }
                }
            } else {
                throw Exception()
            }
        } catch (e: Exception) {
            Toast.makeText(this, "Not a CTS pallet.", Toast.LENGTH_SHORT).show()
            //make error sound
            val toneGen = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
            toneGen.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT, 200)
        }
    }


    fun onClickBackToHome(view: View) {
        val myIntent = Intent(
            this,
            ContainerMenu::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }
}
