package nl.cts.tools;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class CtsTools {
    @Context private HttpServletRequest rq;
	
	public CtsTools(){}
	
    public static String fileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        return new String(encoded, StandardCharsets.US_ASCII);
    }

    public static boolean isLive(ServletContext sc) throws Exception {
    	try{
	    	String env = sc.getInitParameter("env");
	    	return (env != null && env.equalsIgnoreCase("production"));
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new Exception("Cannot get environment variable or system error.");
    	}
    }

    public static String getEnv(ServletContext sc) throws Exception {
    	try{
	    	return sc.getInitParameter("env");
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new Exception("Cannot get environment variable or system error.");
    	}
    }

    public static int getEnvLevel(ServletContext sc, int defaultLocal) throws Exception {
    	try{
	    	String env = sc.getInitParameter("env");
	    	
	    	if(env == null) return defaultLocal;
	    	
	    	if(env.equalsIgnoreCase("development")){
	    		return 1;
	    	}else if(env.equalsIgnoreCase("production")){
	    		return 2;
	    	}else{
	    		return defaultLocal;
	    	}
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new Exception("Cannot get environment variable or system error.");
    	}
    }

    public static int getEnvLevel(ServletContext sc) throws Exception {
    	try{
	    	String env = sc.getInitParameter("env");
	    	
	    	if(env == null) return 0;
	    	
	    	if(env.equalsIgnoreCase("development")){
	    		return 1;
	    	}else if(env.equalsIgnoreCase("production")){
	    		return 2;
	    	}else{
	    		return 0;
	    	}
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new Exception("Cannot get environment variable or system error.");
    	}
    }
    
    public static DateTime addBusinessDays(DateTime dt, Integer days){
    	Integer i = 0;
    	while(i < days){
    		dt = dt.plusDays(1); 
    		if(dt.getDayOfWeek()<=5) i++;
    	}
    	return dt;
    }
    
    public static JsonArray mergeJsonArray(JsonArray a1, JsonArray a2) throws Exception {
    	JsonArray r = new JsonArray();
        for(JsonElement je: a1){ r.add(je); }
        for(JsonElement je: a2){ r.add(je); }
        return r;
    }
    
    @RequiresApi(api = Build.VERSION_CODES.O)
	public static String toOracleDate(String date) throws DateTimeException{
    	if(date.length() != 10){
    		throw new DateTimeException("Invalid date format. DD-MM-YYYY requested");
    	}
		String[] odate = date.split("-");
		return odate[2]+"-"+odate[1]+"-"+odate[0];
    }
    
    /**
     * Simple method to clean a string of empty values and empty spaces
     * 
     * @param value String
     * @return String
     */
	public static String clean(String value){
		if(value == null || value.equals("null")) return ""; //If empty, return empty
		return value.trim(); //trim uitvoeren om overbodige spaties te verwijderen
	}
}
