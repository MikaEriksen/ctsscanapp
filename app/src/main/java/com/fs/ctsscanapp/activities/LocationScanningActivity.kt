package com.fs.ctsscanapp.activities

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.MenuItem
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.Response
import com.fs.ctsscanapp.services.ConverterData.fromJsonString
import com.fs.ctsscanapp.services.Rhea
import kotlinx.android.synthetic.main.activity_location_scanning.*

class LocationScanningActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_scanning)

        runOnUiThread{
            textView29.text = UserName.user
        }
    }

    fun onClickBackToMenu(view: View) {
        val myIntent: Intent = Intent(
            this@LocationScanningActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@LocationScanningActivity.startActivity(myIntent)
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.characters != null && event.characters.isNotEmpty()) {
            try {
                val scannedText = event.characters.toString()

                //Regex match A-z 3, -, match any number
                if (Regex("^[A-z]{3}[-]\\S+").containsMatchIn(scannedText)) {
                    locationText.text = scannedText
                    locationText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 46F)
                    scannedTextbox.text = "Please scan SSCC"
                } else if (Regex("^\\d+").containsMatchIn(scannedText)) {
                    scannedTextbox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20F)
                    scannedTextbox.text = scannedText
                    checkmarkView.visibility = ImageView.INVISIBLE

                    Toast.makeText(
                        this,
                        "Getting data...",
                        Toast.LENGTH_SHORT
                    ).show()

                    AsyncTask.execute {
                        val response: String? = Rhea().sendLocation(
                            location = locationText.text.toString(),
                            scannedBarcode =  scannedText,
                            user =  UserName.user
                        )

                        val locResponse: Response = fromJsonString(response)
                        if (locResponse.status) {
                            runOnUiThread {
                                scannedTextbox.text = "Dosvlg: " + locResponse.dosvlg
                                scannedTextbox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 36F)
                                checkmarkView.setImageResource(R.drawable.ic_check_green_50dp)
                                checkmarkView.visibility = ImageView.VISIBLE
                            }
                        } else {
                            runOnUiThread {
                                Toast.makeText(
                                    this@LocationScanningActivity,
                                    locResponse.opmerking.toString(),
                                    Toast.LENGTH_LONG
                                ).show()

                                checkmarkView.setImageResource(R.drawable.ic_close_black_24dp)
                                checkmarkView.visibility = ImageView.VISIBLE
                            }
                        }
                        Thread.sleep(1000)
                        runOnUiThread {
                            checkmarkView.visibility = ImageView.INVISIBLE
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            } finally {
                return super.dispatchKeyEvent(event)
            }
        }
        return super.dispatchKeyEvent(event)
    }
}
