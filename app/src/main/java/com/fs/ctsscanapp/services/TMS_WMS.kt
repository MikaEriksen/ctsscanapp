package com.fs.ctsscanapp.services

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.*
import com.fs.ctsscanapp.activities.MainActivity
import com.fs.ctsscanapp.activities.MenuActivity
import kotlinx.android.synthetic.main.activity_tms__wms.*

class TMS_WMS : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tms__wms)

        runOnUiThread {
            textView32.text = UserName.user
        }
    }

    fun onClickTMS(view: View) {
        MenuItem.TmsOrWms = "tms"
        val myIntent = Intent(
            this@TMS_WMS,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@TMS_WMS.startActivity(myIntent)
    }

    fun onClickWMS(view: View) {
        MenuItem.TmsOrWms = "wms"
        val myIntent = Intent(
            this@TMS_WMS,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@TMS_WMS.startActivity(myIntent)
    }

    fun onClickBackToLogin(view: View) {
        val myIntent = Intent(
            this@TMS_WMS,
            MainActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@TMS_WMS.startActivity(myIntent)
    }
}
