// To parse the JSON, install jackson-module-kotlin and do:
//
//   val containerRelatieList = ContainerRelatieList.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.*
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.module.kotlin.*

val mapperCr = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class ContainerRelatieList (
    val message: String? = null,
    val data: List<Datum>? = null
) {
    fun toJson() = mapperCr.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapperCr.readValue<ContainerRelatieList>(json)
    }
}

data class Datum (
    val id: Long? = null,

    @get:JsonProperty("ora_naam1")@field:JsonProperty("ora_naam1")
    val oraNaam1: String? = null
)
