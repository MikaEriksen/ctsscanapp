// To parse the JSON, install Klaxon and do:
//
//   val welcome3 = Welcome3.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.beust.klaxon.*

private val klaxon = Klaxon()

data class SoapToObjectGetRitten (
    @Json(name = "soap:Envelope")
    val soapEnvelope: SoapEnvelopeGetRittenResponse
) {
    fun toJson() = klaxon.toJsonString(this)

    companion object {
        fun fromJson(json: String) = klaxon.parse<SoapToObjectGetRitten>(json)
    }
}

data class SoapEnvelopeGetRittenResponse (
    @Json(name = "xmlns:soap")
    val xmlnsSoap: String,

    @Json(name = "xmlns:xsd")
    val xmlnsXSD: String,

    @Json(name = "soap:Body")
    val soapBody: SoapBodyGetRittenResponse,

    @Json(name = "xmlns:xsi")
    val xmlnsXsi: String
)

data class SoapBodyGetRittenResponse (
    @Json(name = "GetRittenResponse")
    val getRittenResponse: GetRittenResponse
)

data class GetRittenResponse (
    @Json(name = "GetRittenResult")
    val getRittenResult: GetRittenResult,

    val xmlns: String
)

data class GetRittenResult (
    @Json(name = "ViewRitInfo")
    val viewRitInfo: List<ViewRitInfo>
)

data class ViewRitInfo (
    @Json(name = "AantalGelost")
    val aantalGelost: String,

    @Json(name = "RitNummer")
    val ritNummer: String,

    @Json(name = "ChauffeurNaam")
    val chauffeurNaam: String? = null,

    @Json(name = "ZendingAantal")
    val zendingAantal: String,

    @Json(name = "ControleGetal")
    val controleGetal: String,

    @Json(name = "AankomstTijd")
    val aankomstTijd: String? = null,

    @Json(name = "Omschrijving")
    val omschrijving: String,

    @Json(name = "AantalGeladen")
    val aantalGeladen: String,

    @Json(name = "Kenteken")
    val kenteken: String? = null,

    @Json(name = "CharterNaam")
    val charterNaam: String,

    @Json(name = "ETAAankomstTijd")
    val etaAankomstTijd: String? = null,

    @Json(name = "VerwerkingsStatus")
    val verwerkingsStatus: String,

    @Json(name = "ColliAantal")
    val colliAantal: String,

    @Json(name = "VertrekTijd")
    val vertrekTijd: String? = null,

    @Json(name = "ZendingAantal405")
    val zendingAantal405: String,

    @Json(name = "LaatsteTenTDatumTijd")
    val laatsteTenTDatumTijd: String,

    @Json(name = "KentekenOplegger")
    val kentekenOplegger: String? = null,

    @Json(name = "InBehandelingBij")
    val inBehandelingBij: String? = null,

    @Json(name = "DeurNummer")
    val deurNummer: String? = null
)