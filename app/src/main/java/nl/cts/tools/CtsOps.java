package nl.cts.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import nl.cts.logging.CtsLog;

public class CtsOps {
    @Context private HttpServletRequest rq;
	
	public CtsOps(){}
	
	public static String barcodeToDosvlg(String barcode, String env, Connection oracle, Connection mysql, boolean isDosvlg, boolean isLPN, boolean isSSCC, boolean isDosvlgP3, CtsLog log){
		PreparedStatement ps = null;
		List<String> allowed_envs = Arrays.asList("fuf","cef");
		ResultSet rset = null;
		String result = "";
    	
    	try{
    		log.entry("barcodeToDosvlg, start", 0);
    		
    		//Check de env en andere variabelen
    		if(!allowed_envs.contains(env)) {
    			log.entry("barcodeToDosvlg, slechte environment meegegeven (fuf, cef)", 1);
    			return "";
    		}else if(CtsTools.clean(barcode).equals("")){
    			log.entry("barcodeToDosvlg, lege barcode kunnen niet door gaan", 0);
    			return "";
    		}else if(!env.equals("fuf") && !env.equals("cef")){
    			log.entry("barcodeToDosvlg, onbekende waarde voor environment, moet cef of fuf zijn", 0);
    			return "";
    		}
    		
    		//1e check: of het een DOSVLG is (snelste check)
			if(isDosvlg){	
	    		ps = oracle.prepareStatement(
	    			"SELECT tsdsmd.\"dosvlg\" "+
			    	"FROM \""+env+"_tsdsmd\" tsdsmd "+
			    	"WHERE tsdsmd.\"dosvlg\" = ?"
			    );
	    		
	    		ps.setString(1, barcode);
		
		        log.startOracleTime();
		        rset = ps.executeQuery();
		 		log.endOracleTime();
		        
		 		if(rset.next()){
		 			result = rset.getString("dosvlg");
		 			log.entry("barcodeToDosvlg, DOSVLG check, Met barcode: "+barcode+" is volgende dossier gevonden: "+result, 0);
		        }else{
		      	  	log.entry("barcodeToDosvlg, DOSVLG check, Met barcode: "+barcode+" is geen dossier gevonden. We kunnen niks terug geven.", 0);        	  
		        }
			}
    	
    		//2e check: of het een dossier met volgnr(3 chars) betreft (snelste check)
    		if(isDosvlgP3 && result.equals("")){
	    		ps = oracle.prepareStatement(
    				"SELECT tsdsmd.\"dosvlg\" "+
		    		"FROM \""+env+"_tsdsmd\" tsdsmd "+
		    		"WHERE tsdsmd.\"dosvlg\" = ? "
	    		);
	    		
	    		ps.setString(1, barcode.substring(0, barcode.length()-3));
		
		        log.startOracleTime();
		        rset = ps.executeQuery();
		 		log.endOracleTime();
		        
		 		if(rset.next()){
		 			result = rset.getString("dosvlg");
		 			log.entry("barcodeToDosvlg, DOSVLG met volgnr check, Met barcode: "+barcode+" is volgende dossier gevonden: "+result, 0);
		        }else{
		      	  	log.entry("barcodeToDosvlg, DOSVLG met volgnr check, Met barcode: "+barcode+" is geen dossier gevonden.", 0);        	  
		        }
    		}
			
    		//3e check: of het een LPN betreft
    		if(isLPN && result.equals("")){	
        		ps = mysql.prepareStatement(
        			"SELECT dosvlg "+
        			"FROM ffs_uctool.licenseplateregistration "+
        			"WHERE lpn = ? "+
        			"	AND active = 1 "
        		);
        		
	    		ps.setString(1, barcode);
		
		        log.startMysqlTime();
		        rset = ps.executeQuery();
		 		log.endMysqlQuery();
		        
		 		if(rset.next()){
		 			result = rset.getString("dosvlg");
		 			log.entry("barcodeToDosvlg, LPN check, Met barcode: "+barcode+" is volgende dossier gevonden: "+result, 0);
		        }else{
		      	  	log.entry("barcodeToDosvlg, LPN check, Met barcode: "+barcode+" is geen dossier gevonden. We kunnen niks terug geven.", 0);        	  
		        }
    		}
    		
    		//4e check: of het een SSCC is (langzaamste check)
    		if(isSSCC && result.equals("")){
    			ps = oracle.prepareStatement(
					"SELECT tsvvds.\"tsopnr\" "+
		    		"FROM \""+env+"_tsvvds\" tsvvds "+
		    		"WHERE tsvvds.\"tslgun\" = ? "+
		    		"	AND tsvvds.\"vrfilk\" = 1 "+
		    		"	AND tsvvds.\"tsinmd\" = 'd' "+
		    		"	AND tsvvds.\"vmtsrt\" = 51 "
    			);
    			
	    		ps.setString(1, barcode);
		
		        log.startOracleTime();
		        rset = ps.executeQuery();
		 		log.endOracleTime();
		        
		 		if(rset.next()){
		 			result = rset.getString("tsopnr");
		 			log.entry("barcodeToDosvlg, SSCC check, Met barcode: "+barcode+" is volgende dossier gevonden: "+result, 0);
		        }else{
		      	  	log.entry("barcodeToDosvlg, SSCC check, Met barcode: "+barcode+" is geen dossier gevonden.", 0);        	  
		        }
    		}

    		//Close off the rsets and pstates
			try { if(rset != null) rset.close(); } catch (Exception e2) {}
			try { if(ps != null) ps.close(); } catch (Exception e2) {}
    		
    		log.entry("barcodeToDosvlg, end", 0);
    	}catch(Exception e){
    		log.exception("barcodeToDosvlg, catch", e, 1);
    	}finally {
		}
    	
    	return result;
    }
}
