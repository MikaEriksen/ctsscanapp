// To parse the JSON, install jackson-module-kotlin and do:
//
//   val checkLPNModel = CheckLPNModel.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.module.kotlin.*

val checklpnMapper: ObjectMapper = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class CheckLPNModel (
    val message: String? = null,
    val data: Data2? = null
) {
    fun toJson(): String = checklpnMapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = checklpnMapper.readValue<CheckLPNModel>(json)
    }
}

data class Data2 (
    val status: Boolean? = null,
    val opmerking: String? = null,
    val dosvlg: String? = null
)
