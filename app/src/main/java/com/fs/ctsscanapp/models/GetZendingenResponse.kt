// To parse the JSON, install Klaxon and do:
//
//   val welcome4 = Welcome4.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.beust.klaxon.*

private fun <T> Klaxon.convert(k: kotlin.reflect.KClass<*>, fromJson: (JsonValue) -> T, toJson: (T) -> String, isUnion: Boolean = false) =
    this.converter(object: Converter {
        @Suppress("UNCHECKED_CAST")
        override fun toJson(value: Any)        = toJson(value as T)
        override fun fromJson(jv: JsonValue)   = fromJson(jv) as Any
        override fun canConvert(cls: Class<*>) = cls == k.java || (isUnion && cls.superclass == k.java)
    })

private val klaxon = Klaxon()
    .convert(Adr::class,                    { Adr.fromValue(it.string!!) },          { "\"${it.value}\"" })
    .convert(ZendingInfoUnion::class,       { ZendingInfoUnion.fromJson(it) },       { it.toJson() }, true)
    .convert(GoederenRegelInfoUnion::class, { GoederenRegelInfoUnion.fromJson(it) }, { it.toJson() }, true)
    .convert(AfmetingInfoUnion::class,      { AfmetingInfoUnion.fromJson(it) },      { it.toJson() }, true)
    .convert(LaadDatumUnion::class,         { LaadDatumUnion.fromJson(it) },         { it.toJson() }, true)
    .convert(StringUnion::class,            { StringUnion.fromJson(it) },            { it.toJson() }, true)

data class SoapToObjectGetZendingen(
    @Json(name = "soap:Envelope")
    val soapEnvelope: SoapEnvelope
) {
    fun toJson() = klaxon.toJsonString(this)

    companion object {
        fun fromJson(json: String) = klaxon.parse<SoapToObjectGetZendingen>(json)
    }
}

data class SoapEnvelope(
    @Json(name = "xmlns:soap")
    val xmlnsSoap: String,

    @Json(name = "xmlns:xsd")
    val xmlnsXSD: String,

    @Json(name = "soap:Body")
    val soapBody: SoapBody,

    @Json(name = "xmlns:xsi")
    val xmlnsXsi: String
)

data class SoapBody(
    @Json(name = "GetZendingenResponse")
    val getZendingenResponse: GetZendingenResponse
)

data class GetZendingenResponse(
    @Json(name = "GetZendingenResult")
    val getZendingenResult: GetZendingenResult,

    val xmlns: String
)

data class GetZendingenResult(
    @Json(name = "ZendingInfo")
    val zendingInfo: ZendingInfoUnion
)

sealed class ZendingInfoUnion {
    class PurpleZendingInfoValue(val value: PurpleZendingInfo) : ZendingInfoUnion()
    class ZendingInfoElementArrayValue(val value: List<ZendingInfoElement>) : ZendingInfoUnion()

    fun toJson(): String = klaxon.toJsonString(
        when (this) {
            is PurpleZendingInfoValue -> this.value
            is ZendingInfoElementArrayValue -> this.value
        }
    )

    companion object {
        fun fromJson(jv: JsonValue): ZendingInfoUnion = when (jv.inside) {
            is JsonObject -> PurpleZendingInfoValue(jv.obj?.let {
                klaxon.parseFromJsonObject<PurpleZendingInfo>(
                    it
                )
            }!!)
            is JsonArray<*> -> ZendingInfoElementArrayValue(jv.array?.let {
                klaxon.parseFromJsonArray<ZendingInfoElement>(
                    it
                )
            }!!)
            else -> throw IllegalArgumentException()
        }
    }
}

data class ZendingInfoElement(
    @Json(name = "ADR")
    val adr: Adr,

    @Json(name = "VanRitNummer")
    val vanRitNummer: String,

    @Json(name = "AfzenderNaam")
    val afzenderNaam: String,

    @Json(name = "ZendingNummer")
    val zendingNummer: String,

    @Json(name = "IsAfgerond")
    val isAfgerond: String,

    @Json(name = "OntvangerLand")
    val ontvangerLand: String,

    @Json(name = "AfzenderLand")
    val afzenderLand: String,

    @Json(name = "Status")
    val status: String,

    @Json(name = "AantalGeladen")
    val aantalGeladen: String,

    @Json(name = "OntvangerPostcode")
    val ontvangerPostcode: String,

    @Json(name = "GoederenRegels")
    val goederenRegels: PurpleGoederenRegels,

    @Json(name = "DoelRitChauffeurNaam")
    val doelRitChauffeurNaam: String? = null,

    @Json(name = "IsHeeftOverflow")
    val isHeeftOverflow: String,

    @Json(name = "ColliEenheid")
    val colliEenheid: String,

    @Json(name = "Nameten")
    val nameten: Adr? = null,

    @Json(name = "OntvangerStraat")
    val ontvangerStraat: String,

    @Json(name = "AfzenderPlaats")
    val afzenderPlaats: String,

    @Json(name = "VanRitChauffeurNaam")
    val vanRitChauffeurNaam: String,

    @Json(name = "ColliAantal")
    val colliAantal: String,

    @Json(name = "LosDatum")
    val losDatum: String,

    @Json(name = "SsccCodes")
    val ssccCodes: PurpleSsccCodes,

    @Json(name = "IsHeeftPrioriteit")
    val isHeeftPrioriteit: String,

    @Json(name = "AantalGelost")
    val aantalGelost: String,

    @Json(name = "DoelRitNummer")
    val doelRitNummer: String,

    @Json(name = "LaadDatum")
    val laadDatum: LaadDatumUnion,

    @Json(name = "IsHeeftManco")
    val isHeeftManco: String,

    @Json(name = "IsHeeftPrioriteitNummer")
    val isHeeftPrioriteitNummer: String,

    @Json(name = "ZoneLossen")
    val zoneLossen: String,

    @Json(name = "DoelRitAfdeling")
    val doelRitAfdeling: String? = null,

    @Json(name = "OntvangerNaam")
    val ontvangerNaam: String,

    @Json(name = "Referentie")
    val referentie: String,

    @Json(name = "ZendingVolgorde")
    val zendingVolgorde: String,

    @Json(name = "OntvangerPlaats")
    val ontvangerPlaats: String,

    @Json(name = "IsHeeftSchade")
    val isHeeftSchade: String,

    @Json(name = "IsHeeftOpmerking")
    val isHeeftOpmerking: String,

    @Json(name = "ZoneLaden")
    val zoneLaden: String,

    @Json(name = "Locatie")
    val locatie: String,

    @Json(name = "LaadInstructie")
    val laadInstructie: String? = null
)

enum class Adr(val value: String) {
    N("N"),
    Y("Y");

    companion object {
        public fun fromValue(value: String): Adr = when (value) {
            "N" -> N
            "Y" -> Y
            else -> throw IllegalArgumentException()
        }
    }
}

data class PurpleGoederenRegels(
    @Json(name = "GoederenRegelInfo")
    val goederenRegelInfo: GoederenRegelInfoUnion
)

sealed class GoederenRegelInfoUnion {
    class GoederenRegelInfoElementArrayValue(val value: List<GoederenRegelInfoElement>) :
        GoederenRegelInfoUnion()

    class GoederenRegelInfoElementValue(val value: GoederenRegelInfoElement) :
        GoederenRegelInfoUnion()

    public fun toJson(): String = klaxon.toJsonString(
        when (this) {
            is GoederenRegelInfoElementArrayValue -> this.value
            is GoederenRegelInfoElementValue -> this.value
        }
    )

    companion object {
        public fun fromJson(jv: JsonValue): GoederenRegelInfoUnion = when (jv.inside) {
            is JsonArray<*> -> GoederenRegelInfoElementArrayValue(jv.array?.let {
                klaxon.parseFromJsonArray<GoederenRegelInfoElement>(
                    it
                )
            }!!)
            is JsonObject -> GoederenRegelInfoElementValue(jv.obj?.let {
                klaxon.parseFromJsonObject<GoederenRegelInfoElement>(
                    it
                )
            }!!)
            else -> throw IllegalArgumentException()
        }
    }
}

data class GoederenRegelInfoElement(
    @Json(name = "RegelNummer")
    val regelNummer: String,

    @Json(name = "ColliEenheid")
    val colliEenheid: String,

    @Json(name = "Afmetingen")
    val afmetingen: PurpleAfmetingen? = null,

    @Json(name = "BrutoGewicht")
    val brutoGewicht: String,

    @Json(name = "ZendingNummer")
    val zendingNummer: String,

    @Json(name = "VolumeEenheid")
    val volumeEenheid: String,

    @Json(name = "VolumeWaarde")
    val volumeWaarde: String,

    @Json(name = "ColliAantal")
    val colliAantal: String,

    @Json(name = "NietStapelen")
    val nietStapelen: Adr,

    @Json(name = "Commodity")
    val commodity: String,

    @Json(name = "NettoGewicht")
    val nettoGewicht: String,

    @Json(name = "MerkenNummers")
    val merkenNummers: String? = null
)

data class PurpleAfmetingen(
    @Json(name = "AfmetingInfo")
    val afmetingInfo: AfmetingInfoUnion
)

sealed class AfmetingInfoUnion {
    class AfmetingInfoElementArrayValue(val value: List<AfmetingInfoElement>) : AfmetingInfoUnion()
    class AfmetingInfoElementValue(val value: AfmetingInfoElement) : AfmetingInfoUnion()

    public fun toJson(): String = klaxon.toJsonString(
        when (this) {
            is AfmetingInfoElementArrayValue -> this.value
            is AfmetingInfoElementValue -> this.value
        }
    )

    companion object {
        public fun fromJson(jv: JsonValue): AfmetingInfoUnion = when (jv.inside) {
            is JsonArray<*> -> AfmetingInfoElementArrayValue(jv.array?.let {
                klaxon.parseFromJsonArray<AfmetingInfoElement>(
                    it
                )
            }!!)
            is JsonObject -> AfmetingInfoElementValue(jv.obj?.let {
                klaxon.parseFromJsonObject<AfmetingInfoElement>(
                    it
                )
            }!!)
            else -> throw IllegalArgumentException()
        }
    }
}

data class AfmetingInfoElement(
    @Json(name = "VolgNummer")
    val volgNummer: String,

    @Json(name = "ColliAantal")
    val colliAantal: String,

    @Json(name = "RegelNummer")
    val regelNummer: String,

    @Json(name = "Hoogte")
    val hoogte: String,

    @Json(name = "Lengte")
    val lengte: String,

    @Json(name = "ZendingNummer")
    val zendingNummer: String,

    @Json(name = "Breedte")
    val breedte: String
)

sealed class LaadDatumUnion {
    class LaadDatumClassValue(val value: LaadDatumClass) : LaadDatumUnion()
    class StringValue(val value: String) : LaadDatumUnion()

    public fun toJson(): String = klaxon.toJsonString(
        when (this) {
            is LaadDatumClassValue -> this.value
            is StringValue -> this.value
        }
    )

    companion object {
        public fun fromJson(jv: JsonValue): LaadDatumUnion = when (jv.inside) {
            is JsonObject -> LaadDatumClassValue(jv.obj?.let {
                klaxon.parseFromJsonObject<LaadDatumClass>(
                    it
                )
            }!!)
            is String -> StringValue(jv.string!!)
            else -> throw IllegalArgumentException()
        }
    }
}

data class LaadDatumClass(
    @Json(name = "xsi:nil")
    val xsiNil: String
)

data class PurpleSsccCodes(
    val string: StringUnion
)

sealed class StringUnion {
    class StringElementArrayValue(val value: List<StringElement>) : StringUnion()
    class StringValue(val value: String) : StringUnion()

    public fun toJson(): String = klaxon.toJsonString(
        when (this) {
            is StringElementArrayValue -> this.value
            is StringValue -> this.value
        }
    )

    companion object {
        public fun fromJson(jv: JsonValue): StringUnion = when (jv.inside) {
            is JsonArray<*> -> StringElementArrayValue(jv.array?.let {
                klaxon.parseFromJsonArray<StringElement>(
                    it
                )
            }!!)
            is String -> StringValue(jv.string!!)
            else -> throw IllegalArgumentException()
        }
    }
}

data class StringElement(
    val content: String
)

data class PurpleZendingInfo(
    @Json(name = "ADR")
    val adr: Adr,

    @Json(name = "VanRitNummer")
    val vanRitNummer: String,

    @Json(name = "AfzenderNaam")
    val afzenderNaam: String,

    @Json(name = "ZendingNummer")
    val zendingNummer: String,

    @Json(name = "IsAfgerond")
    val isAfgerond: String,

    @Json(name = "OntvangerLand")
    val ontvangerLand: String,

    @Json(name = "AfzenderLand")
    val afzenderLand: String,

    @Json(name = "Status")
    val status: String,

    @Json(name = "AantalGeladen")
    val aantalGeladen: String,

    @Json(name = "OntvangerPostcode")
    val ontvangerPostcode: String,

    @Json(name = "GoederenRegels")
    val goederenRegels: FluffyGoederenRegels,

    @Json(name = "IsHeeftOverflow")
    val isHeeftOverflow: String,

    @Json(name = "ColliEenheid")
    val colliEenheid: String,

    @Json(name = "Nameten")
    val nameten: Adr,

    @Json(name = "OntvangerStraat")
    val ontvangerStraat: String,

    @Json(name = "AfzenderPlaats")
    val afzenderPlaats: String,

    @Json(name = "VanRitChauffeurNaam")
    val vanRitChauffeurNaam: String,

    @Json(name = "ColliAantal")
    val colliAantal: String,

    @Json(name = "LosDatum")
    val losDatum: String,

    @Json(name = "SsccCodes")
    val ssccCodes: FluffySsccCodes,

    @Json(name = "IsHeeftPrioriteit")
    val isHeeftPrioriteit: String,

    @Json(name = "AantalGelost")
    val aantalGelost: String,

    @Json(name = "DoelRitNummer")
    val doelRitNummer: String,

    @Json(name = "LaadDatum")
    val laadDatum: LaadDatumClass,

    @Json(name = "IsHeeftManco")
    val isHeeftManco: String,

    @Json(name = "IsHeeftPrioriteitNummer")
    val isHeeftPrioriteitNummer: String,

    @Json(name = "ZoneLossen")
    val zoneLossen: String,

    @Json(name = "OntvangerNaam")
    val ontvangerNaam: String,

    @Json(name = "Referentie")
    val referentie: String,

    @Json(name = "ZendingVolgorde")
    val zendingVolgorde: String,

    @Json(name = "OntvangerPlaats")
    val ontvangerPlaats: String,

    @Json(name = "IsHeeftSchade")
    val isHeeftSchade: String,

    @Json(name = "IsHeeftOpmerking")
    val isHeeftOpmerking: String,

    @Json(name = "ZoneLaden")
    val zoneLaden: String,

    @Json(name = "Locatie")
    val locatie: String
)

data class FluffyGoederenRegels(
    @Json(name = "GoederenRegelInfo")
    val goederenRegelInfo: GoederenRegelsGoederenRegelInfoClass
)

data class GoederenRegelsGoederenRegelInfoClass(
    @Json(name = "RegelNummer")
    val regelNummer: String,

    @Json(name = "ColliEenheid")
    val colliEenheid: String,

    @Json(name = "Afmetingen")
    val afmetingen: FluffyAfmetingen,

    @Json(name = "BrutoGewicht")
    val brutoGewicht: String,

    @Json(name = "ZendingNummer")
    val zendingNummer: String,

    @Json(name = "VolumeEenheid")
    val volumeEenheid: String,

    @Json(name = "VolumeWaarde")
    val volumeWaarde: String,

    @Json(name = "ColliAantal")
    val colliAantal: String,

    @Json(name = "NietStapelen")
    val nietStapelen: Adr,

    @Json(name = "Commodity")
    val commodity: String,

    @Json(name = "NettoGewicht")
    val nettoGewicht: String
)

data class FluffyAfmetingen(
    @Json(name = "AfmetingInfo")
    val afmetingInfo: AfmetingInfoElement
)

data class FluffySsccCodes(
    val string: String
)