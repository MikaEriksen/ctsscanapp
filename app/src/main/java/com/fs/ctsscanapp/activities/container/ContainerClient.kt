package com.fs.ctsscanapp.activities.container

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.Container
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.ContainerRelatieList
import com.fs.ctsscanapp.services.Japi
import kotlinx.android.synthetic.main.activity_container_client.*

class ContainerClient : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container_client)
        runOnUiThread {
            textView45.text = UserName.user
        }
        getClients()
    }

    private var containerRelatieList: ContainerRelatieList? = null

    private fun getClients() {
        AsyncTask.execute {
            val response: String? = Japi().getClients()
            containerRelatieList = response?.let {
                ContainerRelatieList.fromJson(
                    it
                )
            }

            val spinnerArray: ArrayList<String> = ArrayList()

            containerRelatieList?.data?.forEach {
                it.oraNaam1?.let { it1 -> spinnerArray.add(it1) }
            }

            runOnUiThread {
                val adapter = ArrayAdapter(
                    this, R.layout.support_simple_spinner_dropdown_item, spinnerArray
                )

                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                val sItems = findViewById<Spinner>(R.id.spinner1)
                sItems.adapter = adapter
            }
        }
    }

    fun onClickStartScanning(view: View) {

        if (spinner1.selectedItem.toString() == "Empty") {
            return
        }

        Container.client = "Client: " + spinner1.selectedItem.toString()
        Container.clientid = containerRelatieList?.data?.get(spinner1.selectedItemPosition)?.id

        //Switch to Container Insert
        val myIntent = Intent(
            this,
            ContainerNumber::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }

    fun onClickBackToMenu(view: View) {
        val myIntent = Intent(
            this@ContainerClient,
            ContainerMenu::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@ContainerClient.startActivity(myIntent)
    }
}
