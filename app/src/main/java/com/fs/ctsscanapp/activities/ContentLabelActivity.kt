package com.fs.ctsscanapp.activities

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.Response
import com.fs.ctsscanapp.services.ConverterData.fromJsonString
import com.fs.ctsscanapp.services.Rhea
import kotlinx.android.synthetic.main.activity_content_label.*

class ContentLabelActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_label)

        runOnUiThread{
            textView41.text = UserName.user
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.characters == null || event.characters.isEmpty())
            return super.dispatchKeyEvent(event)

        val scannedsscc = event.characters

        if (Regex("^\\d+").containsMatchIn(scannedsscc)) {
            textView4.text = scannedsscc
        }

        return super.dispatchKeyEvent(event)
    }

    fun onPrintButton(view: View) {
        if (textView4.text.toString().isBlank() || textView4.text.toString().contains("Barcode")) {
            Toast.makeText(this, "SSCC can't be empty", Toast.LENGTH_LONG).show()
            return
        }

        Toast.makeText(this, "Sending...", Toast.LENGTH_SHORT).show()

        //progressBar.isIndeterminate = true
        progressBar.visibility = ProgressBar.VISIBLE

        AsyncTask.execute {
            try {
                val response = Rhea().sendLabel(
                    textView4.text.toString()
                )
                val printResponse: Response = fromJsonString(response)

                if (printResponse.status) {
                    runOnUiThread {
                        Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()
                    }
                } else {
                    runOnUiThread {
                        Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show()
                    }
                }
            } catch (e: Exception) {
                runOnUiThread {
                    Toast.makeText(this, "Exception thrown", Toast.LENGTH_LONG).show()
                }
            } finally {
                runOnUiThread {
                    progressBar.visibility = ProgressBar.GONE
                    textView4.text = "Scan barcode to print"
                }
            }
        }
    }

    fun onClickBackToMenu(view: View) {
        val myIntent = Intent(
            this,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }
}
