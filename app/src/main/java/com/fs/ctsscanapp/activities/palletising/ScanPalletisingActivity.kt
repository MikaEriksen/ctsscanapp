package com.fs.ctsscanapp.activities.palletising

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.models.NotLinkedModel
import com.fs.ctsscanapp.models.ResponseModel
import com.fs.ctsscanapp.services.Japi
import kotlinx.android.synthetic.main.activity_scan_palletising.*
import org.json.JSONObject

class ScanPalletisingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_palletising)
    }

    fun onClickGoBack(view: View){
        val myIntent = Intent(
            this@ScanPalletisingActivity,
            MenuPalletisingActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@ScanPalletisingActivity.startActivity(myIntent)
    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {

        if (editTextPallet.text.isNullOrBlank() || editTextColli.text.isNullOrBlank())
            return super.dispatchKeyEvent(event)

        if (event != null) {
            if (event.keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_DOWN){
                createLink()
                updateScannedCount(editTextPallet.text.toString())
            }
        }
        return super.dispatchKeyEvent(event)
    }

    private fun updateScannedCount(contentSSCC: String){
        try{
            AsyncTask.execute {
                var response: String = Japi().barcodeToDosvlg(contentSSCC)
                val ob = JSONObject(response)
                response = Japi().notLinked(ob.getLong("data").toString())
                val notLinked: NotLinkedModel? = NotLinkedModel.fromJson(response)

                val total: Int? = notLinked?.data?.totaal?.toInt()
                val ssccs: Int? = notLinked?.data?.ssccs?.size?.let { total?.minus(it) }

                runOnUiThread {
                    textView99.text = "Content labels scanned: ${ssccs}/${total}"
                }
            }
        }catch(e: Exception){
            Toast.makeText(this, "Failed to get scanned count", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateFinishedCount(palletSSCC: String){
        if (palletSSCC.isBlank())
            return

        try{
            AsyncTask.execute {
                val response: String = Japi().getCountIdPallet(palletSSCC)
                Log.i("response", response)
                val parsed: ResponseModel? = ResponseModel.fromJson(response)

                runOnUiThread {
                    if (parsed != null) {
                        textView86.text = "Pallets finished: ${parsed.data?.get(0)?.palletsAantalAfgerond}/${parsed.data?.get(0)?.palletsAantalTotaal}"
                    }
                }
            }
        }catch (e:Exception){
            Toast.makeText(this, "Could not fetch data", Toast.LENGTH_SHORT).show()
        }
    }

    private fun createLink(){
        if (editTextPallet.text.isNullOrBlank() || editTextColli.text.isNullOrBlank()){
            Toast.makeText(this, "Could not be blank!", Toast.LENGTH_SHORT).show()
            return
        }

        val palletSSCC = editTextPallet.text.toString()
        val colliSSCC = editTextColli.text.toString()

        try{
            AsyncTask.execute {
                val response: String = Japi().createLinkPalletLabels(palletSSCC, colliSSCC)
                val resJson: ResponseModel? = ResponseModel.fromJson(response)

                if (resJson != null) {
                    if (!resJson.message.equals("Finished")){
                        runOnUiThread {
                            Toast.makeText(this, "Not linked!", Toast.LENGTH_SHORT).show()
                        }
                    }else{
                        runOnUiThread {
                            editTextColli.text.clear()
                            editTextColli.requestFocus()
                        }
                    }
                }
            }
        }catch (e:Exception){
            Toast.makeText(this, "Not linked!", Toast.LENGTH_SHORT).show()
        }
    }

    fun onClickFinish(view: View){
        try{
            val palletSSCC = editTextPallet.text.toString()
            AsyncTask.execute {
                Japi().finishPallet(palletSSCC)
                runOnUiThread {
                    editTextPallet.text.clear()
                    editTextColli.text.clear()
                    editTextPallet.requestFocus()
                }
            }
            updateFinishedCount(palletSSCC)
        }catch (e:Exception){
            runOnUiThread {
                Toast.makeText(this, "NOT FINISHED", Toast.LENGTH_SHORT).show()
            }
        }
    }
}