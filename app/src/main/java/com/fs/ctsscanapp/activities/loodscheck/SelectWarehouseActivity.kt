package com.fs.ctsscanapp.activities.loodscheck

import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.format.Formatter
import android.util.Log
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.MenuItem
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.activities.MenuActivity
import kotlinx.android.synthetic.main.activity_select_warehouse.*

class SelectWarehouseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_warehouse)

        try {
            val wm: WifiManager =
                applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val ip = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)

            if (Regex("^(192)\\.(168)\\.(138)\\.\\d{1,3}\$").matches(ip)) {
                radioLoc.check(R.id.radioButton2)
            } else {
                radioLoc.check(R.id.radioButton)
            }
        } catch (e: Exception) {

        }
    }


    fun onClickBackToMenu(view: View) {
        val myIntent = Intent(
            this@SelectWarehouseActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@SelectWarehouseActivity.startActivity(myIntent)
    }


    private fun convertWarehouse(warehouse: String): String {
        when (warehouse) {
            "Pudongweg" -> return "PUD"
            "Schillingweg" -> return "SCH"
        }
        return warehouse
    }


    fun onClickConfirmWarehouse(view: View) {
        try {
            val ware: RadioButton = radioLoc.findViewById(radioLoc.checkedRadioButtonId)
            MenuItem.warehouse = convertWarehouse(ware.text.toString())

            val myIntent = Intent(
                this@SelectWarehouseActivity,
                SelectLocationActivity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            this@SelectWarehouseActivity.startActivity(myIntent)
        } catch (e: Exception) {
            Log.e("Error", e.toString())
        }
    }
}
