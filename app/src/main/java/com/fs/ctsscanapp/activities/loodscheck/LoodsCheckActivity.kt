package com.fs.ctsscanapp.activities.loodscheck

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Paint
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import com.fs.ctsscanapp.*
import com.fs.ctsscanapp.models.GetDossierData
import com.fs.ctsscanapp.models.Response
import com.fs.ctsscanapp.services.ConverterData
import com.fs.ctsscanapp.services.Rhea
import kotlinx.android.synthetic.main.activity_loods_check.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class LoodsCheckActivity : AppCompatActivity() {


    private var locScan = HashMap<String, String?>()
    private var tDialog: AlertDialog? = null
    private var isBusy: Boolean = false


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loods_check)

        textView76.text = "Warehouse location: ${MenuItem.warehouse?.let { convertWarehouse(it) }}"
        textView45.text = UserName.user
        textViewCheckLocation.text = "Location: ${MenuItem.scannedLocation}"

        if (MenuItem.inclocation) {
            textViewCheckLocation.visibility = TextView.VISIBLE
        } else {
            textViewCheckLocation.visibility = TextView.GONE
        }
    }


    @SuppressLint("SetTextI18n")
    private fun updateCounter() {
        runOnUiThread {
            textView77.text = "Total scanned: ${listSSCCLayout.childCount}"
        }
    }


    private fun convertWarehouse(warehouse: String): String {
        when (warehouse) {
            "Pudongweg" -> return "PUD"
            "Schillingweg" -> return "SCH"
            "PUD" -> return "Pudongweg"
            "SCH" -> return "Schillingweg"
        }
        return warehouse
    }


    fun onClickBackToMenu(view: View) {
        runOnUiThread {
            val myIntent = Intent(
                this@LoodsCheckActivity,
                SelectLocationActivity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            this@LoodsCheckActivity.startActivity(myIntent)
        }
    }

    fun onClickEndSession(view: View) {
        AsyncTask.execute {
            if (!endSession()){
                runOnUiThread {
                    Toast.makeText(this, "Session did not end!", Toast.LENGTH_LONG).show()
                }
            }else{
                runOnUiThread {
                    Toast.makeText(this, "Session ended", Toast.LENGTH_SHORT).show()
                    val myIntent = Intent(
                        this@LoodsCheckActivity,
                        SelectLocationActivity::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    this@LoodsCheckActivity.startActivity(myIntent)
                }
            }
        }
    }


    private fun checkBarcode(text: String): Boolean {
        return Regex(pattern = "^\\d{6,20}$").matches(text)
    }


    private fun checkLocation(text: String): Boolean {
        return Regex("^[A-z]{3} \\d{6}$").matches(text) ||
                Regex("^\\D\\d*$").matches(text) ||
                text == "sch vloer" ||
                text == "pud vloer"
    }


    private fun checkConnectivity(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetwork
        val isConnected = activeNetwork != null
        if (!isConnected) {
            Toast.makeText(this, "CHECK WIFI", Toast.LENGTH_SHORT).show()
        }
        return isConnected
    }


    private fun addTextview(scanned: String, location: String?): Boolean {
        return if (!locScan.containsKey(scanned)) {
            locScan[scanned] = location
            val text = TextView(this)
            text.textSize = 24F
            text.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
            text.text = scanned
            text.tag = scanned
            listSSCCLayout.addView(text, 0)
            updateCounter()
            true
        } else {
            false
        }
    }


    private fun changeTextToGreen(scanned: String) {
        runOnUiThread {
            val view =
                this.listSSCCLayout.findViewWithTag<TextView>(scanned)
            view.setTextColor(ColorStateList.valueOf(Color.GREEN))
            listSSCCLayout.removeView(view)
            listSSCCLayout.addView(view)
        }
    }


    private fun createViewForDialog(
        text: String,
        size: Float = 18F,
        color: Int? = null,
        underline: Boolean = false,
        lines: Int = 1
    ): TextView {
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        val tv = TextView(this)
        tv.text = text
        tv.paintFlags = if (underline) Paint.UNDERLINE_TEXT_FLAG else tv.paintFlags
        tv.maxLines = lines
        if (color != null) {
            tv.setTextColor(color)
        }
        tv.ellipsize = TextUtils.TruncateAt.END
        tv.layoutParams = params
        tv.textSize = size

        if (tv.text.isEmpty()) {
            tv.visibility = TextView.GONE
        }
        return tv
    }


    private fun showSSCCEditText() {
        try {
            val v2 = this.tDialog?.findViewById<LinearLayout>(R.id.layout4)
            v2?.visibility = LinearLayout.VISIBLE
            val v3 = this.tDialog?.findViewById<EditText>(R.id.layout3)
            v3?.requestFocus()
        } catch (e: Exception) {

        }
    }


    private fun createInputVeld(knownlocation: String? = null, isSSCC: Boolean = false): LinearLayout {
        val params1 = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        val params2 = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        val linearLayout = LinearLayout(this)
        linearLayout.id = if (isSSCC) R.id.layout4 else R.id.layout2
        linearLayout.orientation = LinearLayout.HORIZONTAL

        val tv = TextView(this)
        tv.text = if (isSSCC) "SSCC: " else "Barcode: "
        tv.setTextColor(Color.WHITE)
        tv.layoutParams = params1
        tv.textSize = 18F

        linearLayout.addView(tv, 0)

        when {
            knownlocation != null -> {
                tv.text = "Location: $knownlocation"
            }
            isSSCC -> {
                val et = EditText(this)
                et.hint = "00321..."
                et.layoutParams = params2
                et.id = R.id.layout3
                et.maxLines = 1
                linearLayout.visibility = LinearLayout.GONE
                linearLayout.addView(et, 1)
            }
            else -> {
                val et = EditText(this)
                et.layoutParams = params2
                et.hint = "00321... / pud 27..."
                et.id = R.id.layout1
                et.maxLines = 1
                linearLayout.addView(et, 1)
            }
        }
        return linearLayout
    }


    private fun inputToView(linearLayout: LinearLayout, text: String) {
        try {
            val v = this.tDialog?.findViewById<LinearLayout>(R.id.layout2)
            linearLayout.removeView(v)
            linearLayout.addView(createViewForDialog(text))

            val v2 = this.tDialog?.findViewById<LinearLayout>(R.id.layout4)
            v2?.visibility = LinearLayout.VISIBLE

            val v3 = this.tDialog?.findViewById<EditText>(R.id.layout3)
            v3?.requestFocus()
        } catch (e: Exception) {

        }
    }


    private fun createLine(ll: LinearLayout, title: String, value: String, dubbleline: Boolean = false) {
        ll.addView(createViewForDialog(title, 10F, underline = true))
        ll.addView(createViewForDialog(value, color = Color.WHITE, lines = if (dubbleline) 2 else 1))
    }


    private fun showInfoScherm(scanned: String) {
        AsyncTask.execute {
            try {
                val builder = AlertDialog.Builder(this)
                val response = Rhea().getDossierData(scanned, UserName.user)
                val dossierData: GetDossierData = GetDossierData.fromJson(response)

                val ll = LinearLayout(this)
                ll.orientation = LinearLayout.VERTICAL
                ll.setPadding(32)

                createLine(ll, "Barcode:", "   $scanned")
                createLine(ll, "File no:", "   ${dossierData.data?.zendingNummer}")
                createLine(
                    ll,
                    "Recipient:",
                    "   ${dossierData.data?.ontvangerNaam}${System.lineSeparator()}   ${dossierData.data?.ontvangerPlaats} (${dossierData.data?.ontvangerLand})",
                    true
                )

                if (!dossierData.data?.laadDatum.equals("-")){
                    dossierData.data?.laadDatum = dossierData.data?.laadDatum?.replaceAfter("T", "")?.removeSuffix("T")
                    val parser = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                    val formatter = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
                    dossierData.data?.laadDatum = formatter.format(parser.parse(dossierData.data?.laadDatum))
                }

                createLine(
                    ll,
                    "Loading trip date / zone:",
                    "   ${
                        dossierData.data?.laadDatum
                    } / ${dossierData.data?.zoneLossen}"
                )
                createLine(
                    ll,
                    "Loading trip / driver:",
                    "   ${dossierData.data?.doelRitNummer} / ${dossierData.data?.doelRitChauffeurNaam}"
                )

                ll.addView(createInputVeld(MenuItem.scannedLocation))
                ll.addView(createInputVeld(isSSCC = true))

                builder.setView(ll)
                builder.setNeutralButton("Close") { _, _ ->
                    //Do Nothing
                }
                builder.setOnKeyListener { _, _, event ->
                    dialogOnDispatch(scanned, ll, event)
                    return@setOnKeyListener dispatchKeyEvent(event)
                }
                runOnUiThread {
                    editText.text.clear()
                    tDialog = builder.create()
                    tDialog?.show()
                    if (MenuItem.scannedLocation != null) {
                        showSSCCEditText()
                    }
                    changeTextToGreen(scanned)
                    tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkGreen)
                }
            } catch (ex: Exception) {
                textToButton(scanned)
                tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkRed)
            }
        }
    }


    @SuppressLint("CutPasteId")
    private fun dialogOnDispatch(scanned: String, ll: LinearLayout, event: KeyEvent) {
        if ((event.keyCode == KeyEvent.KEYCODE_ENTER || event.scanCode == 441) &&
            event.action == KeyEvent.ACTION_UP &&
            event.repeatCount == 0 && !isBusy
        ) {
            isBusy = true

            var builderscanned: String? = null
            try {
                val v = this.tDialog?.findViewById<EditText>(R.id.layout1)
                builderscanned = v?.text.toString().trim()

                if (builderscanned.isNullOrBlank() || builderscanned == "null") {
                    val v2 = this.tDialog?.findViewById<EditText>(R.id.layout3)
                    builderscanned = v2?.text.toString().trim()
                }
            } catch (e: Exception) {

            }

            when {
                builderscanned?.let { checkLocation(it) }!! -> {
                    MenuItem.scannedLocation = builderscanned

                    AsyncTask.execute {
                        try {
                            if (MenuItem.scannedLocation?.let { checkLocation(it) }!!) {
                                //Voeg locatiescan entry toe
                                if (sendLocation(scanned, MenuItem.scannedLocation!!)) {
                                    runOnUiThread {
                                        inputToView(ll, "Location: ${MenuItem.scannedLocation}")
                                        ll.addView(createInputVeld(isSSCC = true))

                                        if (!MenuItem.inclocation)
                                            MenuItem.scannedLocation = null

                                        tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkGreen)
                                        val v1 = this.tDialog?.findViewById<EditText>(R.id.layout1)
                                        v1?.text?.clear()
                                        val v2 = this.tDialog?.findViewById<EditText>(R.id.layout3)
                                        v2?.text?.clear()
                                    }
                                } else {
                                    runOnUiThread {
                                        textToButton(scanned)
                                        tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkRed)
                                    }
                                }
                            } else {
                                tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkRed)
                            }
                        } catch (e: Exception) {
                            tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkRed)
                        }
                    }
                }
                checkBarcode(builderscanned) -> {
                    tDialog?.dismiss()
                    editText.requestFocus()
                    //Toon infoscherm
                    showNext(builderscanned)
                }
                else -> {
                    tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkRed)
                    editText.text.clear()
                    editText.requestFocus()
                }
            }
        }
        isBusy = false
    }

    private fun showNext(scanned: String) {
        editText.text.clear()

        isBusy = true

        //Locatie scan ingesteld?
        try {
            if (MenuItem.scannedLocation?.let { checkLocation(it) }!!) {
                //Voeg locatiescan entry toe
                AsyncTask.execute {
                    if (!sendLocation(scanned, MenuItem.scannedLocation!!)) {
                        textToButton(scanned)
                        runOnUiThread {
                            tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkRed)
                        }
                    } else {
                        runOnUiThread {
                            tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkGreen)
                        }
                        changeTextToGreen(scanned)
                    }
                }
            }
        } catch (e: Exception) {

        }

        //Add info on screen
        addTextview(
            scanned,
            if (MenuItem.scannedLocation.isNullOrEmpty()) "" else MenuItem.scannedLocation
        )

        //Toon infoscherm
        if (MenuItem.warehouse != "SCH"){
            showInfoScherm(scanned)
        }

        //Voeg loodscheck entry toe
        if (MenuItem.warehouse == "PUD"){
            AsyncTask.execute {
                if (!sendInhouse(scanned)) {
                    textToButton(scanned)
                    tDialog?.window?.setBackgroundDrawableResource(R.color.colorDarkRed)
                }
            }
        }

        isBusy = false
    }

    //Global Override
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        checkConnectivity()

        if ((event.keyCode == KeyEvent.KEYCODE_ENTER || event.scanCode == 441) &&
            event.action == KeyEvent.ACTION_UP &&
            event.repeatCount == 0
        ) {
            if (isBusy)
                return false
            val scanned = editText.text.toString().trim()

            //Is volgende scan een dossiernummer?
            when {
                checkBarcode(scanned) -> {
                    showNext(scanned)
                }
                checkLocation(scanned) -> {
                    runOnUiThread {
                        textViewCheckLocation.text = "Location: $scanned"
                        editText.text.clear()
                    }
                    MenuItem.scannedLocation = scanned
                }
                else -> {
                    try {
                        if (event.characters?.let { checkBarcode(it) }!!) {
                            tDialog?.dismiss()
                            editText.requestFocus()
                        }
                    } catch (ex: Exception) {

                    }
                }
            }
        }

        return super.dispatchKeyEvent(event)
    }


    private fun sendInhouse(scanned: String): Boolean {
        return try {
            val response = Rhea().sendInhouse(
                scannedBarcode = scanned,
                user = UserName.user,
                warehouse = MenuItem.warehouse
            )

            val locResponse: Response = ConverterData.fromJsonString(response)
            locResponse.status
        } catch (e: Exception) {
            false
        }
    }


    private fun endSession(): Boolean {
        return try {
            val response = Rhea().endSession(
                user = UserName.user
            )

            val locResponse: Response = ConverterData.fromJsonString(response)
            locResponse.status
        } catch (e: Exception) {
            false
        }
    }


    private fun sendLocation(scanned: String, location: String): Boolean {
        return try {
            val response = Rhea().sendLocation(
                location = location,
                scannedBarcode = scanned,
                user = UserName.user
            )

            val locResponse: Response = ConverterData.fromJsonString(response)
            locResponse.status
        } catch (e: Exception) {
            false
        }
    }


    private fun textToButton(text: String) {
        runOnUiThread {
            val view =
                this.listSSCCLayout.findViewWithTag<TextView>(text)
            listSSCCLayout.removeView(view)
        }

        val but = Button(this)
        but.text = text
        but.tag = text
        but.backgroundTintList = ColorStateList.valueOf(Color.RED)
        but.textSize = 24F
        but.setOnClickListener {
            if (!checkConnectivity())
                return@setOnClickListener

            AsyncTask.execute {
                try {
                    runOnUiThread {
                        but.backgroundTintList = ColorStateList.valueOf(Color.GRAY)
                    }

                    if (locScan[but.tag.toString()]?.let { it1 ->
                            sendLocation(but.tag.toString(), it1)
                        }!!) {
                        runOnUiThread {
                            buttonToGreen(but)
                        }
                    } else {
                        throw Exception()
                    }
                } catch (e: Exception) {
                    runOnUiThread {
                        but.backgroundTintList = ColorStateList.valueOf(Color.RED)
                    }
                }
            }
        }

        runOnUiThread {
            listSSCCLayout.addView(but, 0)
            tDialog?.dismiss()
        }
    }


    private fun buttonToGreen(but: Button) {
        val view =
            this.listSSCCLayout.findViewWithTag<Button>(but.text.toString())
        listSSCCLayout.removeView(view)

        val newview = TextView(this)
        newview.textSize = 24F
        newview.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        newview.text = but.text
        newview.tag = but.text
        newview.setTextColor(ColorStateList.valueOf(Color.GREEN))
        listSSCCLayout.addView(newview)
    }
}