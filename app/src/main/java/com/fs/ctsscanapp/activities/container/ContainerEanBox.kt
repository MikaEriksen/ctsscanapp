package com.fs.ctsscanapp.activities.container

import android.app.AlertDialog
import android.content.Intent
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.AsyncTask
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.Container
import com.fs.ctsscanapp.Pallet
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.DefaultID
import com.fs.ctsscanapp.services.Japi
import kotlinx.android.synthetic.main.activity_container_ean_box.*

class ContainerEanBox : AppCompatActivity() {


    private var isProcessing: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container_ean_box)

        runOnUiThread {
            textView59.text = UserName.user
            textView73.text = Container.client
            textView74.text = "Container: " + Container.containernumber
            textView101.text = "PO: " + Container.ponumber
            textView69.text = "Pallet: " + Container.pallet
            textView82.text = "Total on pallet: ${Pallet.boxcount?: "-"}"
        }
    }


    //TODO extra


    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (isProcessing) {
            return super.dispatchKeyEvent(event)
        }

        try {
            isProcessing = true

            if (event.keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP &&
                event.repeatCount == 0) {
                if (editText4.text.isEmpty() && editText5.text.isEmpty() && editText6.text.isEmpty()){
                    editText4.requestFocus()
                    return super.dispatchKeyEvent(event)
                } else if (editText4.text.isNotEmpty() && editText5.text.isEmpty() && editText6.text.isEmpty()) {
                    editText5.requestFocus()
                    return super.dispatchKeyEvent(event)
                } else if (editText4.text.isNotEmpty() && editText5.text.isNotEmpty() && editText6.text.isEmpty()) {
                    editText6.requestFocus()
                    return super.dispatchKeyEvent(event)
                }

                //Ga door als values kloppen
                if (checkScanCHILD(editText4.text.toString().trim()) &&
                    checkScanEAN(editText5.text.toString().trim()) &&
                    checkScanBOX(editText6.text.toString().trim())
                ) {
                    //Vul direct in
                    Container.childid = editText4.text.toString().trim()
                    Container.ean = editText5.text.toString().trim()
                    Container.fnumber = editText6.text.toString().trim()
                    Container.damaged = spinner.selectedItemPosition <= 0
                    Container.remark = if (editText7.text.isNullOrEmpty()) "-" else editText7.text.toString()

                    //Send it and check response
                    AsyncTask.execute {
                        val response: String = Japi().sendBox()
                        val defaultid: DefaultID = response.let { DefaultID.fromJson(it) }
                        Container.boxid = defaultid.data?.id
                        Container.message = defaultid.message
                        Pallet.boxcount = defaultid.data?.boxCount

                        if (Container.message.equals("Finished")) {
                            runOnUiThread {
                                textView82.text = "Total on pallet: ${Pallet.boxcount?: "-"}"
                                spinner.setSelection(0)
                                editText7.text.clear()
                                editText6.text.clear()
                                editText5.text.clear()
                                editText4.text.clear()
                                editText4.requestFocus()
                            }
                            return@execute
                        }

                        val builder = AlertDialog.Builder(this)
                        builder.setTitle("Mixed?")
                        builder.setMessage("Convert to mixed pallet?")
                            .setPositiveButton(
                                R.string.yes
                            ) { _, _ ->
                                //setMixed
                                AsyncTask.execute {
                                    convertToMixed()
                                }
                            }
                            .setNegativeButton(
                                R.string.no
                            ) { _, _ ->
                                //clear values
                                runOnUiThread {
                                    editText6.text.clear()
                                    editText5.text.clear()
                                    editText4.text.clear()
                                    editText4.requestFocus()
                                }
                            }
                        // Create the AlertDialog object and return it
                        runOnUiThread {
                            builder.create()
                            builder.show()
                        }
                    }
                } else if (checkScanEAN(editText5.text.toString().trim()) &&
                    editText6.text.toString().isEmpty()
                ) {
                    Container.ean = editText5.text.toString().trim()
                    editText6.requestFocus()
                } else if (checkScanBOX(editText6.text.toString().trim()) &&
                    editText5.text.toString().isEmpty()
                ) {
                    Container.fnumber = editText6.text.toString().trim()
                    editText5.requestFocus()
                }
            }
        } catch (e: Exception) {
            //make error sound
            val toneGen = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
            toneGen.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT, 200)
        } finally {
            isProcessing = false
            return super.dispatchKeyEvent(event)
        }
    }


    private fun convertToMixed() {
        val respo: String = Japi().setMixed()
        val defid: DefaultID = respo.let { DefaultID.fromJson(it) }

        if (defid.message.equals("Finished")) {
            val responseBox: String = Japi().sendBox()
            val defaultidBox: DefaultID = responseBox.let { DefaultID.fromJson(it) }
            Container.boxid = defaultidBox.data?.id

            if (defaultidBox.message.equals("Finished")) {
                runOnUiThread {
                    editText6.text.clear()
                    editText5.text.clear()
                    editText4.text.clear()
                    editText4.requestFocus()
                }
            }
        }
    }


    private fun checkScanCHILD(text: String): Boolean {
        return Regex("^22\\d{12}$").matches(text)
    }


    private fun checkScanEAN(text: String): Boolean {
        return Regex("^\\d{13}$").matches(text)
    }


    private fun checkScanBOX(text: String): Boolean {
        return Regex("^(\\S+ \\d+ \\d+)\$").matches(text)
    }


    fun onClickStartPallet(view: View) {
        val myIntent = Intent(
            this,
            ContainerPallet::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }


    fun onClickComplete(view: View) {
        val myIntent = Intent(
            this,
            ContainerComplete::class.java
        )
        this.startActivity(myIntent)
    }


    fun onClickBackToMenu(view: View) {
        val myIntent = Intent(
            this,
            ContainerMenu::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }
}
