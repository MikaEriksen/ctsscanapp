package com.fs.ctsscanapp.activities.loodscheck

import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.format.Formatter
import android.view.KeyEvent
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.MenuItem
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.activities.MenuActivity
import kotlinx.android.synthetic.main.activity_select_location.*
import kotlinx.android.synthetic.main.activity_select_warehouse.*

class SelectLocationActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_location)

        try {
            MenuItem.inclocation = false
            val wm: WifiManager =
                applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val ip = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)

            if (Regex("^(192)\\.(168)\\.(138)\\.\\d{1,3}\$").matches(ip)) {
                radioLoc.check(R.id.radioButton2)
            } else {
                radioLoc.check(R.id.radioButton)
            }
        } catch (e: Exception) {

        }
    }


    fun onClickBackToMenu(view: View) {
        val myIntent = Intent(
            this@SelectLocationActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@SelectLocationActivity.startActivity(myIntent)
    }


    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.keyCode == KeyEvent.KEYCODE_ENTER &&
            event.action == KeyEvent.ACTION_UP &&
            event.repeatCount == 0
        ) {
            val scanned = editTextLocation.text.toString().trim()

            if (checkLocation(scanned)) {
                MenuItem.scannedLocation = scanned
                confirmLocButton.performClick()
            } else {
                MenuItem.scannedLocation = null
            }
        }
        return super.dispatchKeyEvent(event)
    }


    private fun checkLocation(loc: String): Boolean {
        return Regex("^[A-z]{3} \\d*$").matches(loc) ||
                Regex("^\\D\\d*$").matches(loc)
    }


    fun onClickLocYes(view: View) {
        editTextLocation.visibility = TextView.VISIBLE
        MenuItem.inclocation = true
        editTextLocation.requestFocus()
    }


    fun onClickLocNo(view: View) {
        editTextLocation.visibility = TextView.GONE
        MenuItem.inclocation = false
    }


    fun onClickConfirm(view: View) {
        try {
            val selected = radioInc.checkedRadioButtonId == R.id.radioButton6
            val scanned = editTextLocation.text.toString().trim()

            if (MenuItem.inclocation || selected) {
                if (checkLocation(scanned)) {
                    MenuItem.scannedLocation = scanned
                }

                val myIntent = Intent(
                    this@SelectLocationActivity,
                    LoodsCheckActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                this@SelectLocationActivity.startActivity(myIntent)
            } else {
                MenuItem.scannedLocation = null
            }
        } catch (e: Exception) {

        }
    }
}
