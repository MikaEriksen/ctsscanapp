package com.fs.ctsscanapp.activities

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.MenuItem
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.Response
import com.fs.ctsscanapp.services.ConverterData.fromJsonString
import com.fs.ctsscanapp.services.Rhea
import kotlinx.android.synthetic.main.activity_check_location.*

class CheckLocationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_location)

        runOnUiThread {
            textView26.text = UserName.user
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        try {
            if (event.characters.toString().isNotEmpty()) {
                runOnUiThread {
                    textView36.text = event.characters.toString()
                    textView6.text = ""
                    textView34.text = ""
                    textView35.text = ""
                    button2.visibility = Button.GONE
                    button15.visibility = Button.GONE
                }

                AsyncTask.execute {
                    val response: String? = Rhea().getLocation(
                        scannedBarcode = event.characters.toString(),
                        user = UserName.user,
                        tmsorwms = MenuItem.TmsOrWms
                    )

                    val LocationData: Response = fromJsonString(response)//TODO gebruik een ander model (response dinges)

                    runOnUiThread {
                        //set data
                        button2.visibility = Button.VISIBLE
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            return super.dispatchKeyEvent(event)
        }
    }

    fun onClickCancel(view: View) {
        //stuur data terug
    }

    fun onClickOk(view: View) {
        //stuur terug dat alles goed staat
    }

    fun onClickBackToMenu(view: View) {
        val myIntent: Intent = Intent(
            this@CheckLocationActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@CheckLocationActivity.startActivity(myIntent)
    }
}
