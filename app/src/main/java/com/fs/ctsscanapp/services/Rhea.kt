package com.fs.ctsscanapp.services

import android.os.AsyncTask
import android.os.NetworkOnMainThreadException
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL
import java.nio.charset.StandardCharsets
import javax.net.ssl.HttpsURLConnection

class Rhea : AsyncTask<String?, Void?, String>() {


    private val baseUrl = "https://rhea.ctsgroup.nl/webintern/webservices.nsf/"


    fun sendLocation(
        location: String,
        scannedBarcode: String?,
        user: String?
    ): String {
        val urlExtension: String = String.format(
            "Locationscanning_UpdateV2.xsp" +
                    "?location=${location.replace(" ", "_" )}" +
                    "&barcode=$scannedBarcode" +
                    "&user=$user"
        )
        return doInBackground(urlExtension)
    }


    fun endSession(
        user: String?
    ): String {
        val urlExtension: String = String.format(
            "Locationscanning_EndSessionV2.xsp" +
                    "?user=$user"
        )
        return doInBackground(urlExtension)
    }


    fun getDossierData(
        scannedBarcode: String?,
        user: String?
    ): String {
        val urlExtension: String = String.format(
            "Locationscanning_GetDossierData.xsp" +
                    "?barcode=$scannedBarcode" +
                    "&user=$user"
        )
        return doInBackground(urlExtension)
    }


    fun sendInhouse(
        scannedBarcode: String?,
        user: String?,
        warehouse: String?
    ): String {
        val urlExtension: String = String.format(
            "Locationscanning_UpdateInhouse.xsp" +
                    "?barcode=$scannedBarcode" +
                    "&user=$user" +
                    "&warehouse=$warehouse"
        )
        return doInBackground(urlExtension)
    }


    fun getLocation(
        scannedBarcode: String?,
        user: String?,
        tmsorwms: String?
    ): String {
        val urlExtension: String = String.format(
            "Locationscanning_Check.xsp" +
                    "?sscc=$scannedBarcode" +
                    "&user=$user" +
                    "&app=$tmsorwms"
        )
        return doInBackground(urlExtension)
    }


    fun sendLabel(sscc: String?): String {
        val urlExtension: String = String.format(
            "Contentlabel_Print.xsp" +
                    "?sscc=$sscc"
        )
        return doInBackground(urlExtension)
    }


    fun sendFinish(
        dosvlg: String?,
        complete: String?,
        scanned: String?,
        user: String?,
        department: String?
    ): String {
        val urlExtension: String = String.format(
            "Loadtruck_FinishShipment.xsp" +
                    "?dosvlg=$dosvlg" +
                    "&complete=$complete" +
                    "&scanned=$scanned" +
                    "&user=$user" +
                    "&department=$department"
        )
        return doInBackground(urlExtension)
    }


    override fun doInBackground(vararg params: String?): String {
        val linkToSend = URL(baseUrl + params[0])
        val conn: HttpsURLConnection = linkToSend.openConnection() as HttpsURLConnection
        conn.requestMethod = "GET"
        conn.setRequestProperty("Accept-Encoding", "deflate")
        conn.instanceFollowRedirects = true
        try{
            conn.connect()
        } catch (e: NetworkOnMainThreadException) {
            return "NetworkOnMainThreadException"
        }

        val `is`: InputStream = try {
            conn.inputStream
        } catch (e: IOException) {
            conn.errorStream
        }

        //Extract stream data
        val reader = BufferedReader(
            InputStreamReader(
                `is`,
                StandardCharsets.UTF_8
            )
        )

        var data: String? = null
        val stringBuilder = StringBuilder()
        while (true) {
            try {
                if (reader.readLine().also { data = it } == null) break
            } catch (e: IOException) {
                e.printStackTrace()
            }
            stringBuilder.append(data)
        }
        return stringBuilder.toString()
    }
}