package com.fs.ctsscanapp.activities.container

import android.content.Intent
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.AsyncTask
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.Container
import com.fs.ctsscanapp.Pallet
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.DefaultID
import com.fs.ctsscanapp.services.Japi
import kotlinx.android.synthetic.main.activity_container_number.*

class ContainerNumber : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container_number)
        runOnUiThread {
            textView51.text = UserName.user
            textView71.text = Container.client
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        /*try {
            if (event.keyCode == KeyEvent.KEYCODE_ENTER) {
                button27.performClick()
                button27.isEnabled = false
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            button27.isEnabled = true
        }*/
        return super.dispatchKeyEvent(event)
    }

    private fun checkScanContainer(text: String): Boolean {
        return Regex("^[A-Za-z\\d]{4}\\w*").matches(text)
    }

    private fun checkScanPO(text: String): Boolean {
        return Regex("^F\\d*").matches(text)
    }

    fun onClickStartPallet(view: View) {
        try {
            Container.containernumber = editText.text.toString()
            Container.ponumber = editText9.text.toString()

            if (checkScanContainer(Container.containernumber!!) && checkScanPO(Container.ponumber!!)) {
                AsyncTask.execute {
                    val response: String = Japi().sendContainer()
                    val defaultid: DefaultID = response.let { DefaultID.fromJson(it) }
                    Container.containerid = defaultid.data?.id
                }

                val myIntent = Intent(
                    this,
                    ContainerPallet::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                this.startActivity(myIntent)
            } else {
                throw java.lang.Exception()
            }

        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "Invalid.", Toast.LENGTH_SHORT).show()
            //make error sound
            val toneGen = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
            toneGen.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT, 200)
        }
    }

    fun onClickBackToClient(view: View) {
        val myIntent = Intent(
            this,
            ContainerClient::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this.startActivity(myIntent)
    }
}
