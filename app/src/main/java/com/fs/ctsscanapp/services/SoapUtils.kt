package com.fs.ctsscanapp.services


import android.annotation.SuppressLint
import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE
import org.kxml2.kdom.Element
import org.kxml2.kdom.Node
import java.text.SimpleDateFormat
import java.util.*


/*
<ScanSoort>
    RitOntvangen                RitLaden                    ZendingOntvangen
    ZendingLaden                LoodsCheck                  LosseScan
</ScanSoort>

<TrackAndTraceCode>
    NietGespecificeerd          LoodsGelost                 LoodsGeladen
    LoodsGeladenVerkeerdeRit    Overbevinding               Overflow
    GedeeltelijkManco           VolledigManco               Schade
    Opmerking                   AanwezigTijdensLoodsCheck   GereedVoorLaden
    RitGeladen                  VerwachteBinnenkomst        RitGelost
    GoederenRegelAanpassing     InTransit                   OutForDelivery
    PrintenLabels               ShipmentOffloaded           PrintenLabelsDeur1Tot13
    PrintenLabelsDeur14Tot25    ColliCount                  NotLoaded
    NotPresent                  EpRetour                    EpMeegegeven
    MissedConnectionHub         KLAARGZT
</TrackAndTraceCode>

<losDatum>
    2020-12-31
</losDatum>

<ritStatus>
    NotSpecified                GereedVoorLaden             IsGeladen
    VerwachteBinnenkomst        IsGelost
</ritStatus>

<status>
    NietGespecificeerd          GedeeltelijkGescand         WordtGescand
    WordtGescandInactief        Afgerond
</status>

<zendingActie>
    LossenInLoods               LadenInLoods
</zendingActie>
*/


class SoapUtils {
    companion object {
        private const val SOAP_URL =
            "http://192.168.12.171/CTSGroup.Kewill.Webservices.v4/KewillComponents.asmx"
        private const val SOAP_NAMESPACE = "http://webservices.ctsgroup.local/"
        private const val USERNAME = "avscan17"
        private const val PASSWORD = "scan1733"
        private const val CLIENT_NAME = "WMS_App"
        private const val TOKEN = "KewillComponentsAuthenticationToken"
        private const val XML_VERSION = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"


        private fun createHeader(): Array<Element?> {
            val header: Array<Element?> = arrayOfNulls(1)
            header[0] = Element().createElement(SOAP_NAMESPACE, TOKEN)

            val usernameElement: Element = Element().createElement(SOAP_NAMESPACE, "LoginNaam")
            val passwordElement: Element = Element().createElement(SOAP_NAMESPACE, "Wachtwoord")
            val clientElement: Element = Element().createElement(SOAP_NAMESPACE, "ClientName")

            usernameElement.addChild(Node.TEXT, USERNAME)
            passwordElement.addChild(Node.TEXT, PASSWORD)
            clientElement.addChild(Node.TEXT, CLIENT_NAME)

            header[0]?.addChild(Node.ELEMENT, usernameElement)
            header[0]?.addChild(Node.ELEMENT, passwordElement)
            header[0]?.addChild(Node.ELEMENT, clientElement)
            return header
        }


        private fun createEnvelop(soapObject: SoapObject): SoapSerializationEnvelope {
            val envelope = SoapSerializationEnvelope(SoapEnvelope.VER10)
            envelope.dotNet = true
            envelope.implicitTypes = true
            envelope.encodingStyle = "UTF-8"
            envelope.headerOut = createHeader()
            envelope.setOutputSoapObject(soapObject)
            return envelope
        }


        private fun callApi(methodName: String, soapObject: SoapObject): String {
            val soapAction: String = SOAP_NAMESPACE + methodName
            val httpTransportSE = HttpTransportSE(SOAP_URL)
            httpTransportSE.debug = true
            httpTransportSE.setXmlVersionTag(XML_VERSION)
            try {
                httpTransportSE.call(soapAction, createEnvelop(soapObject))
                return httpTransportSE.responseDump.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return ""
        }


        fun getRitten(
            losDatum: String,
            ritStatus: String
        ): String {
            val methodName = "GetRitten"
            val soapObject = SoapObject(SOAP_NAMESPACE, methodName)

            soapObject.addProperty(null, "losDatum", losDatum)
            soapObject.addProperty(null, "ritStatus", ritStatus)

            return callApi(methodName, soapObject)
        }


        fun getZendingen(
            ritNummer: Long,
            zendingActie: String
        ): String {
            val methodName = "GetZendingen"
            val soapObject = SoapObject(SOAP_NAMESPACE, methodName)

            soapObject.addProperty(null, "ritNummer", ritNummer)
            soapObject.addProperty(null, "zendingActie", zendingActie)

            return callApi(methodName, soapObject)
        }
    }
}