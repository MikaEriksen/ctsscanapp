package com.fs.ctsscanapp.models;

import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties
public class Response {
    private boolean status;
    private String opmerking;
    private String dosvlg;
    private String scanStatus;
    private String countUpdatet;

    @JsonProperty("status")
    public boolean getStatus() { return status; }
    @JsonProperty("status")
    public void setStatus(boolean value) { this.status = value; }

    @JsonProperty("opmerking")
    public String getOpmerking() { return opmerking; }
    @JsonProperty("opmerking")
    public void setOpmerking(String value) { this.opmerking = value; }

    @JsonProperty("dosvlg")
    public String getDosvlg() { return dosvlg; }
    @JsonProperty("dosvlg")
    public void setDosvlg(String value) { this.dosvlg = value; }

    @JsonProperty("scan_status")
    public String getScanStatus() { return scanStatus; }
    @JsonProperty("scan_status")
    public void setScanStatus(String value) { this.scanStatus = value; }

    @JsonProperty("count_updatet")
    public String getCountUpdatet() { return countUpdatet; }
    @JsonProperty("count_updatet")
    public void setCountUpdatet(String value) { this.countUpdatet = value; }
}
