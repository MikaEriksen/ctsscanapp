package com.fs.ctsscanapp.activities.palletising

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.activities.MenuActivity
import com.fs.ctsscanapp.activities.ScanActivity

class MenuPalletisingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_palletising)
    }

    fun onClickPrint(view: View){
        val myIntent = Intent(
            this@MenuPalletisingActivity,
            PrintPalletisingActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuPalletisingActivity.startActivity(myIntent)
    }

    fun onClickScan(view: View){
        val myIntent = Intent(
            this@MenuPalletisingActivity,
            ScanPalletisingActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuPalletisingActivity.startActivity(myIntent)
    }

    fun onClickBack(view: View){
        val myIntent = Intent(
            this@MenuPalletisingActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuPalletisingActivity.startActivity(myIntent)
    }
}