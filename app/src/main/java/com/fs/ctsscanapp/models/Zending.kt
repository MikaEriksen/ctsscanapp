// To parse the JSON, install jackson-module-kotlin and do:
//
//   val zending = Zending.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

val mapper = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class Zending (
    val status: Boolean = false,
    val opmerking: String? = null,
    val data: Data? = null
) {
    fun toJson() = mapperDossierData.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapperDossierData.readValue<Zending>(json)
    }
}

data class Data (
    val dosvlg: Long? = null,
    val shiptonaam: String? = null,
    val shiptoadres: String? = null,
    val shiptopostcode: String? = null,
    val shiptostad: String? = null,
    val department: String? = null,
    val shiptoland: String? = null,
    val colkd: String? = null,
    val brgew: Long? = null,
    val aantal: Long? = null,

    @get:JsonProperty("LDS")@field:JsonProperty("LDS")
    val lds: String? = null,

    @get:JsonProperty("SsccList")@field:JsonProperty("SsccList")
    val sscclist: List<String>? = null,

    @get:JsonProperty("DosList")@field:JsonProperty("DosList")
    val doslist: List<Long>? = null,

    @get:JsonProperty("EanList")@field:JsonProperty("EanList")
    val eanList: List<EanList>? = null
)

data class EanList (
    val ean: List<Ean>? = null,
    val artkd: String? = null,
    val count: Long? = null
)

data class Ean (
    val value: String? = null,
    val count: Long? = null,
    val colkd: String? = null
)
