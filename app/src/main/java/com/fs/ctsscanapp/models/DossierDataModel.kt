// To parse the JSON, install jackson-module-kotlin and do:
//
//   val welcome = Welcome.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.module.kotlin.*

val mapperDossierData = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetDossierData (
    val status: Boolean? = null,
    val opmerking: String? = null,
    val data: DossierData? = null,
    val dosvlg: String? = null
) {
    fun toJson() = mapperDossierData.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapperDossierData.readValue<GetDossierData>(json)
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class DossierData (
    @get:JsonProperty("ZendingNummer")@field:JsonProperty("ZendingNummer")
    val zendingNummer: String? = null,

    @get:JsonProperty("OntvangerNaam")@field:JsonProperty("OntvangerNaam")
    val ontvangerNaam: String? = null,

    @get:JsonProperty("OntvangerPlaats")@field:JsonProperty("OntvangerPlaats")
    val ontvangerPlaats: String? = null,

    @get:JsonProperty("OntvangerLand")@field:JsonProperty("OntvangerLand")
    val ontvangerLand: String? = null,

    @get:JsonProperty("LaadDatum")@field:JsonProperty("LaadDatum")
    var laadDatum: String? = null,

    @get:JsonProperty("DoelRitNummer")@field:JsonProperty("DoelRitNummer")
    val doelRitNummer: String? = null,

    @get:JsonProperty("DoelRitChauffeurNaam")@field:JsonProperty("DoelRitChauffeurNaam")
    val doelRitChauffeurNaam: String? = null,

    @get:JsonProperty("ZoneLossen")@field:JsonProperty("ZoneLossen")
    val zoneLossen: String? = null
)
