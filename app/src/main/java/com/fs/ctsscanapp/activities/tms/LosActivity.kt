package com.fs.ctsscanapp.activities.tms


import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.Unload
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.activities.MenuActivity
import com.fs.ctsscanapp.services.SoapUtils
import kotlinx.android.synthetic.main.activity_los_truck.*
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.xml.sax.InputSource
import java.io.StringReader
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory


class LosActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_los_truck)

        textView89.text = UserName.user

        AsyncTask.execute {
            val pattern = "yyyy-MM-dd"
            val simpleDateFormat = SimpleDateFormat(pattern, Locale.ENGLISH)
            simpleDateFormat.timeZone = TimeZone.getTimeZone("CEST")
            Unload.Date = if (Unload.Date.isNullOrEmpty()) simpleDateFormat.format(Date()) else Unload.Date
            val b = findViewById<Button>(R.id.button37)
            b.text = Unload.Date
            Unload.Date?.let { showRitten(it) }
        }
    }


    private fun showBar() {
        val param = TableLayout.LayoutParams(
            TableLayout.LayoutParams.MATCH_PARENT,
            TableLayout.LayoutParams.WRAP_CONTENT
        )
        val pbar = ProgressBar(this)
        pbar.layoutParams = param

        runOnUiThread {
            val ll = findViewById<LinearLayout>(R.id.textLL)
            ll.addView(pbar, 0)
            pbar.visibility = LinearLayout.VISIBLE
        }
    }


    private fun addParams(): LinearLayout.LayoutParams {
        val params = TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT)
        params.weight = 1f
        return params
    }


    private fun createTextView(content: String?): TextView {
        val textparams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        textparams.setMargins(8, 0, 12, 0)

        val tv = TextView(this)
        tv.text = content
        tv.textSize = 14f
        tv.setTextColor(Color.WHITE)
        tv.maxLines = 1
        tv.layoutParams = textparams
        tv.ellipsize = TextUtils.TruncateAt.END

        return tv
    }


    private fun createItem(ritInfo: Element): View {
        val ritnummer = getElementSafe(ritInfo, "RitNummer")
        val omschrijving = getElementSafe(ritInfo, "Omschrijving")
        val vertrektijd = getElementSafe(ritInfo, "VertrekTijd")
        val zendingAantal = getElementSafe(ritInfo, "ZendingAantal")
        val kenteken = getElementSafe(ritInfo, "Kenteken")
        val verwerkingsStatus = getElementSafe(ritInfo, "VerwerkingsStatus")

        val tl = TableLayout(this)

        val tlparam = TableLayout.LayoutParams(
            TableLayout.LayoutParams.MATCH_PARENT,
            TableLayout.LayoutParams.WRAP_CONTENT
        )
        //tlparam.setMargins(0, 8, 0, 8)
        tl.layoutParams = tlparam
        tl.isStretchAllColumns = true

        val tlr = TableRow(this)

        val sll = LinearLayout(this)
        sll.orientation = LinearLayout.VERTICAL
        sll.layoutParams = addParams()
        sll.addView(createTextView(ritnummer))
        sll.addView(createTextView(omschrijving))
        sll.addView(createTextView("Departure time: " + (vertrektijd ?: "")))

        val slr = LinearLayout(this)
        slr.orientation = LinearLayout.VERTICAL
        slr.layoutParams = TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT)
        slr.addView(createTextView("Consignments: $zendingAantal"))
        slr.addView(createTextView("N. Plate: " + (kenteken ?: "")))

        tlr.addView(sll)
        tlr.addView(slr)
        tl.addView(tlr)

        tl.setOnClickListener {
            if (ritnummer != null) {
                RitActivity().setContent(ritnummer.toLong())
            }
            val myIntent = Intent(
                this,
                RitActivity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            this.startActivity(myIntent)
        }

        when (verwerkingsStatus) {
            "GedeeltelijkGescand", "WordtGescandInactief", "WordtGescand" -> {
                tl.background = getDrawable(R.color.colorOrange)
            }
        }

        return tl
    }


    private fun createDivider(): View {
        val param = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        //param.setMargins(8, 8, 8, 8)
        val view = View(this)
        view.background = getDrawable(R.color.colorLightGrey)
        view.minimumHeight = 4
        view.layoutParams = param
        return view
    }


    fun onClickRefresh(view: View) {
        val b = findViewById<Button>(R.id.button37)
        showRitten(b.text.toString())
    }


    private fun responseToDoc(data: String): Document {
        val dbFactory: DocumentBuilderFactory = DocumentBuilderFactory.newInstance()
        val dBuilder: DocumentBuilder = dbFactory.newDocumentBuilder()
        val xmlInput = InputSource(StringReader(data))
        return dBuilder.parse(xmlInput)
    }


    private fun getElementSafe(rit: Element, element: String): String? {
        return try{
            rit.getElementsByTagName(element).item(0).firstChild.nodeValue
        }catch (e: Exception){
            ""
        }
    }


    private fun showRitten(date: String) {
        AsyncTask.execute {
            showBar()
            val response: String =
                SoapUtils.getRitten(
                    date,
                    "VerwachteBinnenkomst"
                ) //2020-07-21

            try {
                val doc = responseToDoc(response)
                Unload.Ritten = doc.getElementsByTagName("GetRittenResult").item(0) as Element

                runOnUiThread {
                    val tll = findViewById<LinearLayout>(R.id.textLL)
                    tll.removeAllViews()

                    val viewRitInfos = Unload.Ritten?.childNodes


                    if (viewRitInfos != null) {
                        for (i in 0 until viewRitInfos.length) {
                            val viewRitInfo: Element = viewRitInfos.item(i) as Element
                            val item = createItem(viewRitInfo)
                            val div = createDivider()
                            tll.addView(item)
                            tll.addView(div)
                        }
                    }
                }
            } catch (e: Exception) {
                runOnUiThread {
                    val tll = findViewById<LinearLayout>(R.id.textLL)
                    tll.removeAllViews()
                    val item = TextView(this)
                    item.text = "Failed to get data"
                    tll.addView(item)
                }
            }
        }
    }


    fun onClickGetRitten(view: View) {
        val builder = AlertDialog.Builder(this)
        val datePicker = DatePicker(this)
        builder.setView(datePicker)
        builder.setMessage("Change loading date")
        builder.setPositiveButton(R.string.yes) { _, _ ->

            Unload.Date = datePicker.year.toString() + "-" +
                    if (datePicker.month > 9) (datePicker.month + 1).toShort() else "0" + (datePicker.month + 1).toShort() + "-" +
                            if (datePicker.dayOfMonth > 9) datePicker.dayOfMonth.toShort() else "0" + datePicker.dayOfMonth.toShort()

            showRitten(date = Unload.Date!!)

            val b = findViewById<Button>(R.id.button37)
            b.text = Unload.Date
        }
        builder.setNegativeButton(R.string.no) { _, _ ->
            //do nothing
        }

        runOnUiThread {
            builder.create()
            builder.show()
        }
    }


    fun onClickBack(view: View) {
        val myIntent = Intent(
            this@LosActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@LosActivity.startActivity(myIntent)
    }
}