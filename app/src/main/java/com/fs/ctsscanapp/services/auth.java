package com.fs.ctsscanapp.services;

import android.os.AsyncTask;
import android.util.Base64;

import com.fs.ctsscanapp.models.AuthRes;
import com.google.gson.JsonObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

public class auth extends AsyncTask<String, Void, String> {

    public String rheaAuth(String username, String password) {
        String BASE_URL = "https://rhea.ctsgroup.nl/webintern/webservices.nsf/";
        String link = BASE_URL + "Loadtruck_Authenticate.xsp";
        return doInBackground(username, password, link);
    }

    @Override
    protected String doInBackground(String... strings) {
        String creds = strings[0] + ":" + strings[1];
        byte[] bytes = Base64.encode(creds.getBytes(), 1 | 2 | 4 | 16);
        String auth = "Basic " + new String(bytes);
        JsonObject ja = new JsonObject();
        ja.addProperty("encoded_credentials", auth);

        URL link = null;
        try {
            link = new URL(strings[2]); //strings[2] = url
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpsURLConnection conn = null;
        try {
            assert link != null;
            conn = (HttpsURLConnection) link.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            assert conn != null;
            conn.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        conn.setRequestProperty("Accept-Encoding", "deflate");
        conn.setRequestProperty("Content-Encoding", "deflate");
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.setFixedLengthStreamingMode(ja.toString().length());
        conn.setInstanceFollowRedirects(true);
        conn.setDoOutput(true);

        try (OutputStream os = new BufferedOutputStream(conn.getOutputStream())) {
            try (BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))) {
                osw.write(ja.toString());
                osw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            conn.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Get incoming stream
        InputStream is = null;
        try{
            is = conn.getInputStream();
        }catch(Exception e){
            try{
                is = conn.getErrorStream();
            }catch(Exception f){

            }
        }

        //Extract stream data
        BufferedReader reader;
        assert is != null;
        reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
        String data = null;
        StringBuilder stringBuilder = new StringBuilder();

        while (true) {
            try {
                if ((data = reader.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            stringBuilder.append(data);
            //stringBuilder.append("\n");
        }

        conn.disconnect();

        return stringBuilder.toString();
    }

    @Override
    protected void onPostExecute(String result) {
        setValue(result);

        try {
            AuthRes data = ConverterAuth.fromJsonString(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String setValue(String result) {
        return result;
    }
}
