package com.fs.ctsscanapp.activities.palletising

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.core.text.isDigitsOnly
import com.beust.klaxon.JsonObject
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.models.ContainerRelatieList
import com.fs.ctsscanapp.models.PrinterModel
import com.fs.ctsscanapp.models.ResponseModel
import com.fs.ctsscanapp.services.Japi
import com.fs.ctsscanapp.services.TMS_WMS
import kotlin.Exception

class PrintPalletisingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print_palletising)

        getPrinters()
    }

    private var printers: PrinterModel? = null

    private fun getPrinters() {
        try {
            AsyncTask.execute {
                val response: String = Japi().getPrinters()
                printers = response.let {
                    PrinterModel.fromJson(
                        it
                    )
                }

                val spinnerArray: ArrayList<String> = ArrayList()

                printers?.printerNames?.forEach {
                    it.let { it1 -> spinnerArray.add(it1) }
                }

                runOnUiThread {
                    val adapter = ArrayAdapter(
                        this, R.layout.support_simple_spinner_dropdown_item, spinnerArray
                    )

                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                    val sItems = findViewById<Spinner>(R.id.spinner2)
                    sItems.adapter = adapter
                }
            }
        } catch (e: Exception) {
            val adapter = ArrayAdapter(
                this, R.layout.support_simple_spinner_dropdown_item, arrayListOf("Could nog get list")
            )
            val sItems = findViewById<Spinner>(R.id.spinner2)
            sItems.adapter = adapter
        }
    }

    fun onClickPrint(view: View) {
        val eDosvlg = findViewById<EditText>(R.id.editTextTextPersonName2)
        val eTotal = findViewById<EditText>(R.id.editTextTextPersonName)
        val sItems = findViewById<Spinner>(R.id.spinner2)

        if (eDosvlg.text.isNullOrBlank() || eTotal.text.isNullOrBlank()) {
            Toast.makeText(this, "Can't be blank!", Toast.LENGTH_SHORT).show()
            return
        }

        if (eDosvlg.text.isDigitsOnly() && eTotal.text.isDigitsOnly()) {
            try{

                //Limit Total tot 99
                var limited: Int = eTotal.text.toString().toInt()
                if (limited >= 100){
                    limited = 99
                }

                AsyncTask.execute {
                    val response: String = Japi().printPalletLabels(
                        eDosvlg.text.toString().toLong(),
                        limited,
                        sItems.selectedItem.toString()
                    )

                    Log.i("response", response)

                    val jRes: ResponseModel? = ResponseModel.fromJson(response)
                    if (jRes != null) {
                        if (!jRes.message.equals("Finished")) {
                            runOnUiThread {
                                Toast.makeText(this, "Print failed!", Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            runOnUiThread {
                                eDosvlg.text.clear()
                                eTotal.text.clear()
                            }
                        }
                    }
                }
            }catch (e: Exception){
                Toast.makeText(this, "Print failed!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Digits only!", Toast.LENGTH_SHORT).show()
        }
    }

    fun onClickBack(view: View) {
        val myIntent = Intent(
            this@PrintPalletisingActivity,
            MenuPalletisingActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@PrintPalletisingActivity.startActivity(myIntent)
    }
}