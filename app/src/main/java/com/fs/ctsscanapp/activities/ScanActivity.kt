package com.fs.ctsscanapp.activities

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.Barcode
import com.fs.ctsscanapp.MenuItem
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import kotlinx.android.synthetic.main.activity_scan_label.*


class ScanActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_label)

        runOnUiThread{
            textView17.text = UserName.user
        }

        when (MenuItem.selected) {
            "tl" -> {
                textView2.text = getString(R.string.truck_load)
            }
            "cc" -> {
                textView2.text = getString(R.string.check_consol)
            }
            "co" -> {
                textView2.text = getString(R.string.check_order)
            }
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (!checkConnectivity())
            return super.dispatchKeyEvent(event)

        if (event.characters.isNullOrEmpty())
            return super.dispatchKeyEvent(event)

        try {
            if (event.characters.toString().isNullOrEmpty()) {
                return super.dispatchKeyEvent(event)
            } else {
                textView6.setText(event.characters.toString())
                sendIt()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            return super.dispatchKeyEvent(event)
        }
    }

    fun onClickSendLabel(view: View) {
        sendIt()
    }

    private fun sendIt() {
        if (textView6.text.toString().isBlank()) {
            Toast.makeText(this, "Field can't be empty.", Toast.LENGTH_LONG).show()
            return
        }

        Barcode.dosvlg = textView6.text.toString()

        val myIntent = Intent(
            this@ScanActivity,
            OrderActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@ScanActivity.startActivity(myIntent)
    }

    fun onClickBackToLogin(view: View) {
        val myIntent = Intent(
            this@ScanActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@ScanActivity.startActivity(myIntent)
    }

    private fun checkConnectivity(): Boolean {
        val cm: ConnectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: Network? = cm.activeNetwork
        val isConnected: Boolean = activeNetwork != null
        if (!isConnected) {
            runOnUiThread {
                Toast.makeText(this, "Check network", Toast.LENGTH_SHORT).show()
            }
        }

        return isConnected
    }
}
