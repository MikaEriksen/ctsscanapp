// To parse the JSON, install jackson-module-kotlin and do:
//
//   val defaultID = DefaultID.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

val mapperId = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class DefaultID(
    val message: String? = null,
    val data: Dataid? = null
) {
    fun toJson(): String = mapperId.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapperId.readValue<DefaultID>(json)
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Dataid(
    val id: Long? = null,
    val createdNew: Boolean? = null,
    val containers_id: Long? = null,
    val identification: String? = null,
    val mixed: Boolean? = null,
    val boxCount: Long? = null
)
