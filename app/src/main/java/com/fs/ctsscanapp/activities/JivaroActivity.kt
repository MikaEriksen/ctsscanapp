package com.fs.ctsscanapp.activities

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.CheckLPNModel
import com.fs.ctsscanapp.models.CheckLabelModel
import com.fs.ctsscanapp.services.Japi
import kotlinx.android.synthetic.main.activity_jivaro.*

class JivaroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jivaro)

        runOnUiThread {
            textView21.text = UserName.user
        }

        manUpdateButton.visibility = Button.INVISIBLE
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.characters != null && event.characters.isNotEmpty()) {

            val scannedText = event.characters.toString()

            textView9.text = scannedText //LPN
            textView12.text = "" //DOSVLG
            textView14.text = "" //TEXT
            manUpdateButton.visibility = Button.INVISIBLE
            manUpdateButton.backgroundTintList =
                this.resources.getColorStateList(R.color.colorLightGrey)

            //Begin met netwerk etc
            AsyncTask.execute {

                //Check voor license
                val responseCheckLPN: String = Japi().checkLPN(
                    scannedText
                )
                val checkResponse: CheckLPNModel = CheckLPNModel.fromJson(responseCheckLPN)
                if (checkResponse.data?.status == true) {
                    if (checkResponse.data.dosvlg?.isNotEmpty() == true) {
                        runOnUiThread {
                            textView12.text = checkResponse.data.dosvlg
                        }

                        //Check voor Label
                        val responseCheckLabel: String = Japi().checkLabel(
                            checkResponse.data.dosvlg
                        )
                        val labelResponse: CheckLabelModel = CheckLabelModel.fromJson(responseCheckLabel)
                        runOnUiThread {
                            if (labelResponse.data?.status == true) {
                                textView14.text = "Is present"
                                manUpdateButton.visibility = Button.INVISIBLE
                            } else {
                                textView14.text = labelResponse.data?.opmerking ?: ""
                                manUpdateButton.visibility = Button.VISIBLE
                            }
                        }
                    } else {
                        runOnUiThread {
                            textView12.text = "Nope"
                        }
                    }
                } else {
                    runOnUiThread {
                        textView9.text = "Try Again"
                    }
                }
            }
        }
        return super.dispatchKeyEvent(event)
    }

    fun onClickmanUpdateButton(view: View) {

        AsyncTask.execute {

            runOnUiThread {
                val updatebutton = view as Button
                updatebutton.isClickable = false
            }

            //override
            Japi().updateLPN(
                textView9.text.toString() //LPN
            )

            //check the override
            val responseCheck: String = Japi().checkLabel(
                textView12.text.toString() //dosvlg
            )

            val labelResponse: CheckLabelModel = CheckLabelModel.fromJson(responseCheck)
            runOnUiThread {
                if (labelResponse.data?.status == true) {
                    textView14.text = "Is present"
                    manUpdateButton.backgroundTintList = ColorStateList.valueOf(Color.GREEN)
                } else {
                    manUpdateButton.backgroundTintList = ColorStateList.valueOf(Color.RED)
                    textView14.text = labelResponse.data?.opmerking ?: ""
                }

                val updatebutton = view as Button
                updatebutton.isClickable = true
            }
        }
    }

    fun onClickBackToHome(view: View) {
        val myIntent = Intent(
            this@JivaroActivity,
            MenuActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@JivaroActivity.startActivity(myIntent)
    }
}
