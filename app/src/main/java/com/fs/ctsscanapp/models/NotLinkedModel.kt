// To parse the JSON, install Klaxon and do:
//
//   val notLinkedModel = NotLinkedModel.fromJson(jsonString)

package com.fs.ctsscanapp.models

import com.beust.klaxon.*

private val klaxon = Klaxon()

data class NotLinkedModel (
    val message: String? = null,
    val data: DataNotLinked? = null
) {
    public fun toJson() = klaxon.toJsonString(this)

    companion object {
        public fun fromJson(json: String) = klaxon.parse<NotLinkedModel>(json)
    }
}

data class DataNotLinked (
    val totaal: String? = null,
    val ssccs: List<String>? = null
)
