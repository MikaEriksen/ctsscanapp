package com.fs.ctsscanapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.fs.ctsscanapp.MenuItem
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.activities.container.ContainerMenu
import com.fs.ctsscanapp.activities.loodscheck.SelectWarehouseActivity
import com.fs.ctsscanapp.activities.palletising.MenuPalletisingActivity
import com.fs.ctsscanapp.activities.palletising.PrintPalletisingActivity
import com.fs.ctsscanapp.activities.tms.LosActivity
import com.fs.ctsscanapp.services.TMS_WMS
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        runOnUiThread {
            textView16.text = UserName.user

            if (MenuItem.TmsOrWms == "tms") {
                textView5.text = getString(R.string.tms)
                //button2.visibility = Button.GONE
                button6.visibility = Button.GONE
                button9.visibility = Button.GONE
                button8.visibility = Button.GONE
                button10.visibility = Button.GONE
                button16.visibility = Button.GONE
                button17.visibility = Button.GONE
                button25.visibility = Button.GONE
                button47.visibility = Button.GONE

            } else if(MenuItem.TmsOrWms == "wms") {
                textView5.text = getString(R.string.wms)
                button11.isEnabled = false //Location Scaning
            }
        }
    }

    fun onClickTruckLoad(view: View) {
        MenuItem.selected = "tl"
        val myIntent = Intent(
            this@MenuActivity,
            ScanActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }

    fun onClickCheckConsol(view: View) {
        MenuItem.selected = "cc"
        val myIntent = Intent(
            this@MenuActivity,
            ScanActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }

    fun onClickCheckLicense(view: View) {
        MenuItem.selected = "cl"
        val myIntent = Intent(
            this@MenuActivity,
            JivaroActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }

    fun onClickCheckOrder(view: View) {
        MenuItem.selected = "co"
        val myIntent = Intent(
            this@MenuActivity,
            ScanActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }

    fun onClickCheckLocation(view: View) {
        MenuItem.selected = "clo"
        val myIntent = Intent(
            this@MenuActivity,
            CheckLocationActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }

    fun onClickLocationScanning(view: View) {
        MenuItem.selected = "ls"
        val myIntent = Intent(
            this@MenuActivity,
            LocationScanningActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }

    fun onClickLoodsCheck(view: View) {
        MenuItem.selected = "clc"
        val myIntent = Intent(
            this@MenuActivity,
            SelectWarehouseActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }

    fun onClickContainerScanning(view: View) {
        MenuItem.selected = "cm"
        val myIntent = Intent(
            this@MenuActivity,
            ContainerMenu::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }


    fun onClickContentLabels(view: View) {
        MenuItem.selected = "cla"
        val myIntent = Intent(
            this@MenuActivity,
            ContentLabelActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }


    fun onClickTests(view: View) {
        MenuItem.selected = "t"
        val myIntent = Intent(
            this@MenuActivity,
            LosActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }


    fun onClickPalletising(view: View) {
        MenuItem.selected = "pal"
        val myIntent = Intent(
            this@MenuActivity,
            MenuPalletisingActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }


    fun onClickBackToLogin(view: View) {
        val myIntent = Intent(
            this@MenuActivity,
            TMS_WMS::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        this@MenuActivity.startActivity(myIntent)
    }
}
