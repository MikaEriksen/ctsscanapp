package com.fs.ctsscanapp.services

import android.os.AsyncTask
import android.util.Log
import com.fs.ctsscanapp.Container
import com.fs.ctsscanapp.UserName
import com.google.gson.JsonObject
import org.json.JSONObject
import java.io.*
import java.net.URL
import java.nio.charset.StandardCharsets
import javax.net.ssl.HttpsURLConnection

class Japi : AsyncTask<String?, Void?, String>() {
    private val baseUrl = "https://japi.ctsgroup.nl/"
    var status = -1
    private val containerAuthKey = "2fbb0479b7fa4fd0a6b3b0732f9805d817179ba1722cace5f7765c9ed30102a5"
    private val fsUCAuthKey = "70684ec61bba34f7b5735daefc792792c60bd041e531eb529ed2f2b682e7f509"
    private val wmsApiAuthKey = "f02abf04ff5881a932e4bb3918d4cd4c2abefe69df5d058f196053ceac943bc2"
    private val jivaroApiAuthKey = "96228778961d4dce8aa518d447981d3a7881a01656ed61930649e3c3b40bc454"


    fun getClients(): String {
        val urlExtension: String = String.format(
            "containerverwerkingapi/relatie/list"
        )
        return doInBackground(urlExtension, containerAuthKey)
    }


    fun addAttachement(jpg: String): String {
        val urlExtension: String = String.format(
            "containerverwerkingapi/attachment/add/${Container.containerid}"
        )

        val json = JSONObject()
        json.put("Base64", jpg)
        val body = json.toString(4)

        return doInBackground(urlExtension, containerAuthKey , "POST", body)
    }


    fun sendComplete(): String {
        val urlExtension: String = String.format(
            "containerverwerkingapi/container/update/"
        )

        val ob = JSONObject()
        ob.put("id", Container.containerid)
        ob.put("labelsreprinted", Container.labelsreprinted)
        ob.put("status", 1)
        val body = ob.toString(4)

        return doInBackground(urlExtension, containerAuthKey, "POST", body)
    }


    fun sendBox(): String {
        val urlExtension: String = String.format(
            "containerverwerkingapi/box/check/"
        )

        val ob = JSONObject()
        ob.put("cont_pal_id", Container.palletid)
        ob.put("child_id", Container.childid)
        ob.put("iden_str", Container.fnumber)
        ob.put("ean_str", Container.ean)
        ob.put("dmg_bool", if (Container.damaged!!) "0" else "1")
        ob.put("rmrk_str", Container.remark)
        val body = ob.toString(4)

        return doInBackground(urlExtension, containerAuthKey, "POST", body)
    }


    fun sendContainer(): String {
        val urlExtension: String = String.format(
            "containerverwerkingapi/container/check/${Container.clientid}/${Container.containernumber}${Container.ponumber}"
        )
        return doInBackground(urlExtension, containerAuthKey)
    }


    fun sendPallet(): String {
        val urlExtension: String = String.format(
            "containerverwerkingapi/pallet/check/${Container.containerid}/${Container.pallet}"
        )
        return doInBackground(urlExtension, containerAuthKey)
    }


    fun setMixed(): String {
        val urlExtension: String = String.format(
            "containerverwerkingapi/pallet/setMixed/${Container.palletid}"
        )
        return doInBackground(urlExtension, containerAuthKey)
    }


    fun getPrinters(): String {
        val urlExtension: String = String.format(
            "fs-uc-api/printer/names/get/"
        )
        return doInBackground(urlExtension, fsUCAuthKey)
    }


    fun createLinkPalletLabels(palletSSCC: String, boxSSCC: String): String {
        val urlExtension: String = String.format(
            "wms_api/pallet/create_link/${palletSSCC}/${boxSSCC}"
        )
        return doInBackground(urlExtension, wmsApiAuthKey)
    }


    fun getCountIdPallet(palletSSCC: String): String {
        val urlExtension: String = String.format(
            "wms_api/pallet/getCountId/${palletSSCC}"
        )
        return doInBackground(urlExtension, wmsApiAuthKey)
    }


    fun finishPallet(palletSSCC: String): String {
        val urlExtension: String = String.format(
            "wms_api/pallet/finish/${palletSSCC}"
        )
        return doInBackground(urlExtension, wmsApiAuthKey)
    }


    fun notLinked(dosvlg: String): String {
        val urlExtension: String = String.format(
            "wms_api/box/get/not_linked/${dosvlg}"
        )
        return doInBackground(urlExtension, wmsApiAuthKey)
    }


    fun LoadTruckGetShipment(dosvlg: String): String {
        val urlExtension: String = String.format(
            "wms_api/loadtruck_getshipment/${dosvlg}"
        )
        return doInBackground(urlExtension, wmsApiAuthKey)
    }


    fun barcodeToDosvlg(barcode: String): String {
        val urlExtension: String = String.format(
            "wms_api/barcode/to_dosvlg/${barcode}/false/false/true/false"
        )

        return doInBackground(urlExtension, wmsApiAuthKey)
    }


    fun printPalletLabels(dosvlg: Long, amount: Int, printer: String): String {
        val urlExtension: String = String.format(
            "wms_api/pallet_labels/generate"
        )

        val json = JSONObject()
        json.put("dossier_number", dosvlg)
        json.put("pallet_quantity", amount)
        json.put("printer_name", printer)

        val body = json.toString(4)
        return doInBackground(urlExtension, wmsApiAuthKey, "POST", body)
    }

    fun addTnT(dosvlg: String, trackCode: String, username: String): String {
        val urlExtension: String = String.format(
            "cts-xdock-api/trackandtrace/add"
        )

        val data = JSONObject()
        data.put("scan_action", "LosseScan")
        data.put("track_code", trackCode)
        data.put("shipment_number", dosvlg)

        val json = JSONObject()
        json.put("username", username)
        json.put("data", data)

        val body = json.toString(4)
        return doInBackground(urlExtension, null, "POST", body)
    }


    fun checkLPN(lpn: String?): String {
        val urlExtension: String = String.format(
            "jivaro-api/check_lpn/$lpn"
        )
        return doInBackground(urlExtension, jivaroApiAuthKey, "GET")
    }


    fun checkLabel(dosvlg: String?): String {
        val urlExtension: String = String.format(
            "jivaro-api/check_label/$dosvlg"
        )
        return doInBackground(urlExtension, jivaroApiAuthKey, "GET")
    }


    fun updateLPN(lpn: String?): String {
        val urlExtension: String = String.format(
            "jivaro-api/update"
        )

        val data = JSONObject()
        data.put("weight", 1)
        data.put("height", 30)
        data.put("LPN", lpn)

        val json = JSONObject()
        json.put("user", UserName.user)
        json.put("data", data)

        val body = json.toString(4)
        return doInBackground(urlExtension, jivaroApiAuthKey, "POST", body)
    }
    

    override fun doInBackground(vararg params: String?): String {
        val linkToSend = URL(baseUrl + params[0])
        val conn: HttpsURLConnection = linkToSend.openConnection() as HttpsURLConnection
        conn.requestMethod = if(params.getOrNull(2) != null) params[2] else "GET"
        conn.setRequestProperty(
            "Authorization",
            params[1]
        )
        conn.setRequestProperty("Accept-Encoding", "deflate")
        conn.setRequestProperty("Content-Type", "application/json")
        conn.instanceFollowRedirects = true
        Log.i("URL", linkToSend.toString())

        if (conn.requestMethod == "POST"){
            // Send the JSON we created
            val outputStreamWriter = OutputStreamWriter(conn.outputStream)
            outputStreamWriter.write(params[3])
            outputStreamWriter.flush()
        }

        conn.connect()

        val instream: InputStream = try {
            conn.inputStream
        } catch (e: IOException) {
            conn.errorStream
        }

        status = conn.responseCode

        //Extract stream data
        val reader = BufferedReader(
            InputStreamReader(
                instream,
                StandardCharsets.UTF_8
            )
        )

        var data: String? = null
        val stringBuilder = StringBuilder()
        while (true) {
            try {
                if (reader.readLine().also { data = it } == null) break
            } catch (e: IOException) {
                e.printStackTrace()
            }
            stringBuilder.append(data)
        }
        return stringBuilder.toString()
    }
}