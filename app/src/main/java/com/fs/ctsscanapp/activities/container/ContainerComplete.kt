package com.fs.ctsscanapp.activities.container

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.ToneGenerator
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider.getUriForFile
import com.fs.ctsscanapp.Container
import com.fs.ctsscanapp.Photo
import com.fs.ctsscanapp.R
import com.fs.ctsscanapp.UserName
import com.fs.ctsscanapp.models.DefaultID
import com.fs.ctsscanapp.services.Japi
import kotlinx.android.synthetic.main.activity_container_complete.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

class ContainerComplete : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container_complete)

        runOnUiThread {
            textView56.text = UserName.user
            textView70.text = "Container: " + Container.containernumber
        }
    }

    fun onClickTakePic(view: View) {
        Log.i("foto", "button click")
        dispatchTakePictureIntent()
    }

    private fun dispatchTakePictureIntent() {
        Log.i("foto", "dispatchTakePictureIntent")
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    Log.e("foto", "IOException")
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri =
                        getUriForFile(this, "com.fs.ctsscanapp.fileprovider", it)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, Photo.REQUEST_TAKE_PHOTO)
                }
            }
        }
    }


    private fun createImageFile(): File {
        Log.i("foto", "createImageFile")
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            if (storageDir != null) {
                Photo.currentPhotoPath = absolutePath
            }
        }
    }


    private fun galleryAddPic() {
        Log.i("foto", "galleryAddPic")
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(Photo.currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            sendBroadcast(mediaScanIntent)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Photo.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val f = File(Photo.currentPhotoPath)
            val photoURI: Uri =
                getUriForFile(this, "com.fs.ctsscanapp.fileprovider", f)
            val imageStream: InputStream? = contentResolver.openInputStream(photoURI)
            val selectedImage: Bitmap = BitmapFactory.decodeStream(imageStream)
            val baos = ByteArrayOutputStream()
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, baos)
            val b: ByteArray = baos.toByteArray()
            val encodedImage: String = Base64.encodeToString(b, 1 or 2 or 4 or 16)

            AsyncTask.execute {
                galleryAddPic()
                val asd: String = Japi().addAttachement(encodedImage)
                val res: DefaultID = asd.let { DefaultID.fromJson(it) }
                Container.boxid = res.data?.id
                Container.message = res.message

                if (Container.message.equals("Finished")) {
                    runOnUiThread {
                        Toast.makeText(this, res.message.toString(), Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        } else {
            runOnUiThread {
                Toast.makeText(this, "Not saved", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }


    fun onClickComplete(view: View) {
        try {
            AsyncTask.execute {
                Container.labelsreprinted =
                    if (editText8.text.toString().isNullOrEmpty()) 0 else editText8.text.toString()
                        .toLong()

                val response: String = Japi().sendComplete()
                val defaultid: DefaultID = response.let { DefaultID.fromJson(it) }
                if (defaultid.message.equals("Finished")) {
                    val myIntent: Intent = Intent(
                        this@ContainerComplete,
                        ContainerNumber::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    this@ContainerComplete.startActivity(myIntent)
                } else {
                    runOnUiThread {
                        Toast.makeText(this, defaultid.message.toString(), Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        } catch (e: Exception) {
            //make error sound
            val toneGen = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
            toneGen.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT, 200)
        }
    }

    fun onClickBackToMenu(view: View) {
        val myIntent: Intent = Intent(
            this@ContainerComplete,
            ContainerMenu::class.java
        )
        this@ContainerComplete.startActivity(myIntent)
    }

    fun onClickBackToScanning(view: View) {
        val myIntent: Intent = Intent(
            this@ContainerComplete,
            ContainerEanBox::class.java
        )
        this@ContainerComplete.startActivity(myIntent)
    }
}
